{
    "id": "db98f273-3b5b-4977-bab2-d474ab6f8584",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03b4e91e-3d59-4a54-9d93-183d2f1ab95a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db98f273-3b5b-4977-bab2-d474ab6f8584",
            "compositeImage": {
                "id": "64697098-e8d1-4e33-9293-730afe9ed4cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b4e91e-3d59-4a54-9d93-183d2f1ab95a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a056d62-7256-456a-b176-6ae09a5d6707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b4e91e-3d59-4a54-9d93-183d2f1ab95a",
                    "LayerId": "256d77ea-9b43-4251-ad04-30f1e34a2d1e"
                }
            ]
        },
        {
            "id": "89e90a50-d587-4eb0-bda8-39faf5820c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db98f273-3b5b-4977-bab2-d474ab6f8584",
            "compositeImage": {
                "id": "8239ba7e-833b-4a34-b722-f6dbffad51b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e90a50-d587-4eb0-bda8-39faf5820c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df1e0b70-dd10-4834-ba59-d918d3dea47e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e90a50-d587-4eb0-bda8-39faf5820c05",
                    "LayerId": "256d77ea-9b43-4251-ad04-30f1e34a2d1e"
                }
            ]
        },
        {
            "id": "a5e512b9-3fec-4aa1-95d0-a2ce65eca757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db98f273-3b5b-4977-bab2-d474ab6f8584",
            "compositeImage": {
                "id": "3a437900-3d4e-43c6-a34d-290aa6553500",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5e512b9-3fec-4aa1-95d0-a2ce65eca757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "653479a3-0046-417e-b26b-02e3cad933d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5e512b9-3fec-4aa1-95d0-a2ce65eca757",
                    "LayerId": "256d77ea-9b43-4251-ad04-30f1e34a2d1e"
                }
            ]
        },
        {
            "id": "4c5fc114-f15a-463b-93e9-fef50c7e6b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db98f273-3b5b-4977-bab2-d474ab6f8584",
            "compositeImage": {
                "id": "b60987a2-e741-4d21-8bbb-2c745e9f9fca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5fc114-f15a-463b-93e9-fef50c7e6b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9adb2bb9-ba6d-414f-ac6b-f17c76d9504b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5fc114-f15a-463b-93e9-fef50c7e6b47",
                    "LayerId": "256d77ea-9b43-4251-ad04-30f1e34a2d1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "256d77ea-9b43-4251-ad04-30f1e34a2d1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db98f273-3b5b-4977-bab2-d474ab6f8584",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}