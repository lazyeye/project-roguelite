{
    "id": "8de1a955-0de4-4d9b-a46a-85237e80ddb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 3,
    "bbox_right": 10,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a80064f-0d20-4929-9637-100bd8512580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8de1a955-0de4-4d9b-a46a-85237e80ddb5",
            "compositeImage": {
                "id": "23a56249-ff3d-4928-8e39-9d1e303f27c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a80064f-0d20-4929-9637-100bd8512580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a86be8-4de8-4015-81f5-60537928f433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a80064f-0d20-4929-9637-100bd8512580",
                    "LayerId": "a0cc05df-ebb8-4282-8241-aacde53cf55c"
                }
            ]
        },
        {
            "id": "ad33ad9f-66fc-467c-8dac-d047123ecef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8de1a955-0de4-4d9b-a46a-85237e80ddb5",
            "compositeImage": {
                "id": "d7c60781-46fa-4b01-8dd0-69393ca03c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad33ad9f-66fc-467c-8dac-d047123ecef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fc12304-ac7b-4613-9afc-f1dc89f86a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad33ad9f-66fc-467c-8dac-d047123ecef7",
                    "LayerId": "a0cc05df-ebb8-4282-8241-aacde53cf55c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "a0cc05df-ebb8-4282-8241-aacde53cf55c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8de1a955-0de4-4d9b-a46a-85237e80ddb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}