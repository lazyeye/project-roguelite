{
    "id": "fa909367-d543-4102-97db-81a4f65422b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 3,
    "bbox_right": 9,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58b8daae-b17a-4a4b-8367-978ddf3e70fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "949c695f-6f4f-406d-bbae-7cbb49b023a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b8daae-b17a-4a4b-8367-978ddf3e70fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "698c2dc1-2d6a-49b1-b8db-0beb5ce35c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b8daae-b17a-4a4b-8367-978ddf3e70fe",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "0fa02929-6b31-4d19-85cc-f246909960a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "016e794e-cf7b-4714-ac79-b42c47a6597c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa02929-6b31-4d19-85cc-f246909960a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6864a581-e7cd-4ba6-91a1-65c0fe8b531d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa02929-6b31-4d19-85cc-f246909960a7",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "2d5bd572-3f40-4b59-a9a0-a3b6e11b0c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "c9570743-16e7-4e72-b503-df7569fcbac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5bd572-3f40-4b59-a9a0-a3b6e11b0c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7f93668-9304-428d-9371-ec7a98f53a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5bd572-3f40-4b59-a9a0-a3b6e11b0c56",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "c9794338-ac47-4fcc-89b3-460ad1e1f69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "db11e2d6-bc70-46ca-8e51-6bdd09bd6cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9794338-ac47-4fcc-89b3-460ad1e1f69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4e73ef-be58-4b09-9b69-4ff62a553eb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9794338-ac47-4fcc-89b3-460ad1e1f69c",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "9076b276-3490-4740-ae2b-3dde9d3e4f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "cdf8a4e4-f6e2-4306-b30b-38488b7154c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9076b276-3490-4740-ae2b-3dde9d3e4f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee35eb62-c776-4bf6-8e4d-fa72524dc19a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9076b276-3490-4740-ae2b-3dde9d3e4f2b",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "5bdcc4e0-e287-417a-8be8-6a2145e94849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "6218420f-f630-479e-98e1-b460c597f5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bdcc4e0-e287-417a-8be8-6a2145e94849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e18c37-ab6f-4309-b64e-34a7511c62f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bdcc4e0-e287-417a-8be8-6a2145e94849",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "bf44f7ba-5feb-48b3-800a-9f875abf0b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "ed86b493-f9fc-4d64-ad73-51ce5b34546d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf44f7ba-5feb-48b3-800a-9f875abf0b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c2cc2ad-fca6-41fa-acbb-ef8cb5d03fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf44f7ba-5feb-48b3-800a-9f875abf0b06",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "759fc648-2148-4b2a-9a2c-8c6fbc77f755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "40dd5380-0b77-489e-98d5-58cf3eae1554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "759fc648-2148-4b2a-9a2c-8c6fbc77f755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acd4da63-90ac-4913-af60-139e6374e7c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "759fc648-2148-4b2a-9a2c-8c6fbc77f755",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "608c8335-9efa-4d0a-acb3-976b1b6aa909",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "ae6da978-14d1-4835-93f8-6b342152fa82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "608c8335-9efa-4d0a-acb3-976b1b6aa909",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9883f2de-4c3b-4ee2-b4e5-3dfad8103377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "608c8335-9efa-4d0a-acb3-976b1b6aa909",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "6a4840b9-3d35-41ea-b929-b0ea86cce3b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "d74296ef-108c-409b-aadd-40f2978f6257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a4840b9-3d35-41ea-b929-b0ea86cce3b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "930a4783-4243-4784-833a-a173413b0391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a4840b9-3d35-41ea-b929-b0ea86cce3b1",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "7282caaa-1137-4876-9ca8-21c8e575cf20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "be3db60b-d026-4b16-92f9-c27da94a0a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7282caaa-1137-4876-9ca8-21c8e575cf20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29ce09cc-750f-45a7-9157-e3fdba58f03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7282caaa-1137-4876-9ca8-21c8e575cf20",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "f3a816dc-9840-46e6-8cb9-c1c813c8d87e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "b24f49de-15e6-492a-be9b-02fb8d90ccd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a816dc-9840-46e6-8cb9-c1c813c8d87e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d29d80-8578-454b-8bc1-f0b608d36775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a816dc-9840-46e6-8cb9-c1c813c8d87e",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "c2022c16-fa5f-4fe2-bd7e-781dcde0b152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "bc4651f4-78ba-45d9-a08a-2d5527f45b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2022c16-fa5f-4fe2-bd7e-781dcde0b152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "850906eb-e00c-445f-9601-0be5bb10492c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2022c16-fa5f-4fe2-bd7e-781dcde0b152",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "60557e62-0f05-4b47-a855-58582f8b778f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "9529acb2-f2ea-4e30-9acb-734da998ec1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60557e62-0f05-4b47-a855-58582f8b778f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c338339a-3849-462e-ad8c-29871cf17363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60557e62-0f05-4b47-a855-58582f8b778f",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "0ef4830c-7dd1-4e39-887f-6154f9fb02d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "12c3e2fd-2134-457a-85a6-7641cd57d995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef4830c-7dd1-4e39-887f-6154f9fb02d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa220fa9-5e20-46dc-9082-da093cb7906f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef4830c-7dd1-4e39-887f-6154f9fb02d3",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        },
        {
            "id": "3a73db14-87fc-4791-b2ee-5b3875ceaf86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "compositeImage": {
                "id": "50544e42-b958-45c9-95f8-6c99c8cfd3d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a73db14-87fc-4791-b2ee-5b3875ceaf86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df671947-a63c-4c67-87d1-86b3934269a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a73db14-87fc-4791-b2ee-5b3875ceaf86",
                    "LayerId": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "7b35ecb6-46c8-4c3a-b69b-104f16aec9f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa909367-d543-4102-97db-81a4f65422b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}