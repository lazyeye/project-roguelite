{
    "id": "180d5e04-d9e4-481d-a99f-146a6a5688b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_medium_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 4,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e51cf76-99c5-4af2-9494-6cf0473402b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "180d5e04-d9e4-481d-a99f-146a6a5688b5",
            "compositeImage": {
                "id": "df66af5b-139c-4499-9d3b-ac194c834483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e51cf76-99c5-4af2-9494-6cf0473402b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83490151-9e9c-4de7-975b-0aabf0264687",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e51cf76-99c5-4af2-9494-6cf0473402b1",
                    "LayerId": "c5b8994d-85d6-4321-9e11-94e8579f63bb"
                }
            ]
        },
        {
            "id": "79b1d836-d457-45c5-bc13-20db10b91a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "180d5e04-d9e4-481d-a99f-146a6a5688b5",
            "compositeImage": {
                "id": "95ac0180-4615-41a5-a376-96dca5bb864d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b1d836-d457-45c5-bc13-20db10b91a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ebd44b3-dd21-4ad6-b506-c6a2c573b50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b1d836-d457-45c5-bc13-20db10b91a41",
                    "LayerId": "c5b8994d-85d6-4321-9e11-94e8579f63bb"
                }
            ]
        },
        {
            "id": "8c2ec2ee-11e2-4589-9db0-1572af08439f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "180d5e04-d9e4-481d-a99f-146a6a5688b5",
            "compositeImage": {
                "id": "43ecec89-867d-4d87-a7aa-24acedd8be3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2ec2ee-11e2-4589-9db0-1572af08439f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bf9893a-e84b-4d13-85c3-5c81fc9d7cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2ec2ee-11e2-4589-9db0-1572af08439f",
                    "LayerId": "c5b8994d-85d6-4321-9e11-94e8579f63bb"
                }
            ]
        },
        {
            "id": "1bdd39e8-8638-4925-b0ea-9a03f55ec7b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "180d5e04-d9e4-481d-a99f-146a6a5688b5",
            "compositeImage": {
                "id": "58fa9b05-d932-43b5-abff-966a393205a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bdd39e8-8638-4925-b0ea-9a03f55ec7b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c7bde15-aadf-468a-8c22-835286963413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bdd39e8-8638-4925-b0ea-9a03f55ec7b2",
                    "LayerId": "c5b8994d-85d6-4321-9e11-94e8579f63bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "c5b8994d-85d6-4321-9e11-94e8579f63bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "180d5e04-d9e4-481d-a99f-146a6a5688b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}