{
    "id": "3c25b25c-8084-4530-8511-4df748737071",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_spiky_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 4,
    "bbox_right": 9,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cacbc4f5-f00f-4b20-8d49-e7c352b71851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c25b25c-8084-4530-8511-4df748737071",
            "compositeImage": {
                "id": "7f2c4b9d-531c-47b4-94dc-90aeef697e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cacbc4f5-f00f-4b20-8d49-e7c352b71851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcf29d4-1485-45e0-bf8e-aa440129bbbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cacbc4f5-f00f-4b20-8d49-e7c352b71851",
                    "LayerId": "075ff393-061a-409d-9873-909c2498bc6d"
                }
            ]
        },
        {
            "id": "27f45d32-2b15-4ab6-a365-e6b621b628d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c25b25c-8084-4530-8511-4df748737071",
            "compositeImage": {
                "id": "5e63f4d8-d0ab-4c96-9e4d-2c40f4cfa13f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27f45d32-2b15-4ab6-a365-e6b621b628d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4d2485-6971-4420-9ae4-b4b48b1d3c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27f45d32-2b15-4ab6-a365-e6b621b628d5",
                    "LayerId": "075ff393-061a-409d-9873-909c2498bc6d"
                }
            ]
        },
        {
            "id": "d56496f2-6e1b-4856-ac58-affc8885e468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c25b25c-8084-4530-8511-4df748737071",
            "compositeImage": {
                "id": "7b29a148-9758-4c02-a251-1aeb75898098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56496f2-6e1b-4856-ac58-affc8885e468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63eab484-eb9c-4c59-af3a-5835e32cf3c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56496f2-6e1b-4856-ac58-affc8885e468",
                    "LayerId": "075ff393-061a-409d-9873-909c2498bc6d"
                }
            ]
        },
        {
            "id": "2f3a3d20-310c-4393-a456-3f5c68e2776e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c25b25c-8084-4530-8511-4df748737071",
            "compositeImage": {
                "id": "5ec4d683-a667-449d-b00d-c89b85026b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3a3d20-310c-4393-a456-3f5c68e2776e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0503aa79-224b-4714-95e1-a70a25cc21cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3a3d20-310c-4393-a456-3f5c68e2776e",
                    "LayerId": "075ff393-061a-409d-9873-909c2498bc6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "075ff393-061a-409d-9873-909c2498bc6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c25b25c-8084-4530-8511-4df748737071",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}