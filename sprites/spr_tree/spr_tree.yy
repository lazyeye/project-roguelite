{
    "id": "8e067347-7ee6-4d92-817d-46b45e5c8f74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fcb9eed-f3ef-4744-bc68-fb4c3a226985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e067347-7ee6-4d92-817d-46b45e5c8f74",
            "compositeImage": {
                "id": "d94f2401-5ca5-4cd0-a57d-3b7b240abe81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcb9eed-f3ef-4744-bc68-fb4c3a226985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9861098b-2154-4eea-9bc1-64b0230bf3db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcb9eed-f3ef-4744-bc68-fb4c3a226985",
                    "LayerId": "dedf1307-405a-4b1c-81a6-e2a93be7793f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "dedf1307-405a-4b1c-81a6-e2a93be7793f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e067347-7ee6-4d92-817d-46b45e5c8f74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}