{
    "id": "751c58c0-6acf-4507-a1cc-7861aa642aa5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_medium_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 5,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7217ec45-e8ba-44e8-8740-5b4a4bf70c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751c58c0-6acf-4507-a1cc-7861aa642aa5",
            "compositeImage": {
                "id": "d06f0681-73da-412f-9f8c-d215b65fb6e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7217ec45-e8ba-44e8-8740-5b4a4bf70c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57dd79f0-9202-48b8-a8cb-ececbde3ee81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7217ec45-e8ba-44e8-8740-5b4a4bf70c05",
                    "LayerId": "e0885b76-3993-4e77-a2b3-a9fc5d4a2dda"
                }
            ]
        },
        {
            "id": "6ca24c4a-2225-4632-a829-ebae4e448bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751c58c0-6acf-4507-a1cc-7861aa642aa5",
            "compositeImage": {
                "id": "2b27889d-2497-48a5-a74b-e949439284b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca24c4a-2225-4632-a829-ebae4e448bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8937e3bd-5b97-4b5e-bace-3b357496e107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca24c4a-2225-4632-a829-ebae4e448bfe",
                    "LayerId": "e0885b76-3993-4e77-a2b3-a9fc5d4a2dda"
                }
            ]
        },
        {
            "id": "627f35d8-a8bd-44f7-83e6-7d439e816062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751c58c0-6acf-4507-a1cc-7861aa642aa5",
            "compositeImage": {
                "id": "a67ad7b5-f9cd-4cc5-9643-b7f2cecd7a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "627f35d8-a8bd-44f7-83e6-7d439e816062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ae2f22-2acb-438f-adcb-4819c81147a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "627f35d8-a8bd-44f7-83e6-7d439e816062",
                    "LayerId": "e0885b76-3993-4e77-a2b3-a9fc5d4a2dda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "e0885b76-3993-4e77-a2b3-a9fc5d4a2dda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "751c58c0-6acf-4507-a1cc-7861aa642aa5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}