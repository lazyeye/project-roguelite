{
    "id": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_leather_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a5b392d-5586-4883-929b-f6efcdbc9c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "f9c94a54-3465-4e81-abe3-0ca8393ce19e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5b392d-5586-4883-929b-f6efcdbc9c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180c6f20-e508-4a54-9b72-ff31c964aa3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5b392d-5586-4883-929b-f6efcdbc9c23",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "32d39136-3641-44c9-9120-ae1f01d2f2df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "48cb95b4-74cb-46a5-882d-33297e9d6acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32d39136-3641-44c9-9120-ae1f01d2f2df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e143cbda-1898-4b41-8769-38f109c53854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d39136-3641-44c9-9120-ae1f01d2f2df",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "59b4c48a-80a3-44b6-9efc-600d0ace5834",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "9e414796-bdc6-4806-a0fe-9a29bc690a33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b4c48a-80a3-44b6-9efc-600d0ace5834",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f098b25-7395-47b6-ab59-d7ae88e4bc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b4c48a-80a3-44b6-9efc-600d0ace5834",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "d45509c8-7645-4e46-8296-70a8e40235c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "a22316db-6ff6-4da4-bc14-033d77937ebd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d45509c8-7645-4e46-8296-70a8e40235c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82604dd4-bbbe-4995-abc1-8566cd2d58e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45509c8-7645-4e46-8296-70a8e40235c6",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "647618f7-e855-4c86-b8dc-2655591f477c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "f67dd270-5147-4e31-a70d-8de3aeff29d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647618f7-e855-4c86-b8dc-2655591f477c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f358341-699b-493e-9903-47fe17ad58b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647618f7-e855-4c86-b8dc-2655591f477c",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "f1074e58-591b-42a0-a550-0e6b4f7a8482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "9e9f1c66-e35b-4235-a024-bcfb07b91222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1074e58-591b-42a0-a550-0e6b4f7a8482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "363d614c-af5b-4b3b-834b-d86a3f876fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1074e58-591b-42a0-a550-0e6b4f7a8482",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "ed24710a-c1a0-4d3e-8a87-e0e7fe62567e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "ed8401bd-661c-4dee-8eec-8f853d925fc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed24710a-c1a0-4d3e-8a87-e0e7fe62567e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95eebd59-a10b-46cc-9973-9d3416cb0381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed24710a-c1a0-4d3e-8a87-e0e7fe62567e",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "958d0b23-178f-4c59-9eb5-55c197e08a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "cfe1db37-8cc1-4e89-a983-502c228eb801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "958d0b23-178f-4c59-9eb5-55c197e08a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77961078-56a9-4a0f-934a-a334a6ba3bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "958d0b23-178f-4c59-9eb5-55c197e08a5d",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "49b80fc8-c854-43aa-8d58-f97c55b19407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "83f8fb23-dcfa-4fd6-ab33-d05f470cefc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b80fc8-c854-43aa-8d58-f97c55b19407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "334bca72-9613-46d3-b300-d7a135d147ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b80fc8-c854-43aa-8d58-f97c55b19407",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "68ebaf55-79b2-4a6e-960a-8295275def9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "b93bedba-2b4d-47e7-99fe-f73451900a00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68ebaf55-79b2-4a6e-960a-8295275def9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3054b51-68f4-4948-a72b-b39743e96edd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68ebaf55-79b2-4a6e-960a-8295275def9b",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "2439072d-0fb6-43dc-89f5-79a12b316e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "36871851-9faf-464f-ba76-40e925ad1a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2439072d-0fb6-43dc-89f5-79a12b316e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f79b6160-574b-4eba-b8fe-d455e1e32ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2439072d-0fb6-43dc-89f5-79a12b316e5c",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "f9941c9c-9804-416e-a353-1ad321e78006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "6a0563c7-87c1-43d0-b64e-fcb63b69007a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9941c9c-9804-416e-a353-1ad321e78006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74afd30-8685-4676-969a-f2a0dbfe495f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9941c9c-9804-416e-a353-1ad321e78006",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "85737d20-bbc0-410a-813a-5dba9c6c2969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "60aa8f8e-a4b8-4c4d-9ac1-a729e77e98da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85737d20-bbc0-410a-813a-5dba9c6c2969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba7031c-c94f-49e0-955c-3e20558377d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85737d20-bbc0-410a-813a-5dba9c6c2969",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "a6ad499f-e0b2-45f8-8890-4c7a12267c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "14202c3d-dff0-42aa-a10b-ae6479a5897d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ad499f-e0b2-45f8-8890-4c7a12267c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dcb32a5-1803-47a0-9392-845edc806ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ad499f-e0b2-45f8-8890-4c7a12267c05",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "a92913b0-7515-43ca-9beb-cb6261cc5584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "70096d57-040a-4bdc-86a6-6d2334b4df0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92913b0-7515-43ca-9beb-cb6261cc5584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf9f0b61-9237-43dc-80cd-e10aa49ee355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92913b0-7515-43ca-9beb-cb6261cc5584",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        },
        {
            "id": "6e110929-4435-4941-ac16-f570d8e857cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "compositeImage": {
                "id": "ac39fc43-707d-42e0-a2d3-acd55bac24a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e110929-4435-4941-ac16-f570d8e857cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9209c4bc-204d-4231-90c5-8cb625094611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e110929-4435-4941-ac16-f570d8e857cb",
                    "LayerId": "b0a1dc20-035b-4417-94a5-74f47a15445e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b0a1dc20-035b-4417-94a5-74f47a15445e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0eac1e3-76ea-4121-a3de-9ea4c7dcc82e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}