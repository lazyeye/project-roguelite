{
    "id": "b3621cce-5c70-45ae-9163-c95e727ee062",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e278391-51bb-411f-adb1-4996f3e93e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3621cce-5c70-45ae-9163-c95e727ee062",
            "compositeImage": {
                "id": "5b24bfdf-bf5c-4f77-aeb8-d3085e575b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e278391-51bb-411f-adb1-4996f3e93e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44e371c-5dce-4afd-b7d1-5d2583ad713d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e278391-51bb-411f-adb1-4996f3e93e2b",
                    "LayerId": "24f566e9-146d-4bba-ac55-60c090f961ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "24f566e9-146d-4bba-ac55-60c090f961ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3621cce-5c70-45ae-9163-c95e727ee062",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 0,
    "yorig": 0
}