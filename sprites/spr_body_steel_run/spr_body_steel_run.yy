{
    "id": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_steel_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb379639-0179-4a6f-9707-ddd2e2918652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "0d7939ef-f3d8-494e-b583-e2c63ddecc84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb379639-0179-4a6f-9707-ddd2e2918652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d118202-a74c-4f97-9524-31da630130d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb379639-0179-4a6f-9707-ddd2e2918652",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "40fc6802-dcd0-4571-9708-6df2e15bd6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "5c2a67fe-15a4-4886-b2fc-d0ecbcd13846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40fc6802-dcd0-4571-9708-6df2e15bd6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0cbe242-fc09-4bbe-a2d1-5dae869ab316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40fc6802-dcd0-4571-9708-6df2e15bd6a0",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "bd3db46d-4ebe-46c4-b37b-faf274c4a5dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "cf188d48-4149-4334-81d5-00fc978d8f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd3db46d-4ebe-46c4-b37b-faf274c4a5dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8215786b-8774-4be6-b525-9fd09dbc1b6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd3db46d-4ebe-46c4-b37b-faf274c4a5dc",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "a071f6e6-cb59-4704-a20c-37d4effd5da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "195d3dde-7e01-4db6-b2c1-dfc2e2a0d72c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a071f6e6-cb59-4704-a20c-37d4effd5da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf4e598-c505-4c23-86e9-85ef92fe731c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a071f6e6-cb59-4704-a20c-37d4effd5da2",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "a8103a72-458b-443b-b52f-ebd7bdb493df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "b101e01b-c76d-4d79-85d9-881a3a126ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8103a72-458b-443b-b52f-ebd7bdb493df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e48cdc-9f48-4a6c-98fb-3aa59cf31c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8103a72-458b-443b-b52f-ebd7bdb493df",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "a84dc642-61ca-4388-be59-1969f7c55aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "0f5a5543-acbd-4a0b-8baf-2a5de1437e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84dc642-61ca-4388-be59-1969f7c55aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4713e9ba-69f0-47b4-a852-02bd8adccbaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84dc642-61ca-4388-be59-1969f7c55aea",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "6f1517f4-cd72-4dbe-a7e8-8fee4218b021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "acb30350-5f1d-4ebf-8bf3-3bfbd60163cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f1517f4-cd72-4dbe-a7e8-8fee4218b021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e27519-a116-442a-b1f2-2f2b10911100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f1517f4-cd72-4dbe-a7e8-8fee4218b021",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "61ba4a99-188c-408b-91dd-7333632df174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "79fb9659-553c-4551-82f7-db6792faa527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61ba4a99-188c-408b-91dd-7333632df174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4689ae2-786e-45ea-97aa-bfbc13c8fccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61ba4a99-188c-408b-91dd-7333632df174",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "5fbab772-0e1e-493c-8a17-9e3a8e169929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "f775136a-0f2b-46ab-96cf-8bfe1fcf4a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbab772-0e1e-493c-8a17-9e3a8e169929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e1915bb-d807-459c-a2a7-016076a41613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbab772-0e1e-493c-8a17-9e3a8e169929",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "2d5cc6ca-70dc-4d19-8229-7ec381dd76fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "42e7e552-cf4d-4917-bb15-0b76ffb5e81b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5cc6ca-70dc-4d19-8229-7ec381dd76fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54b820f4-43ff-4fda-8c6e-fae41318cb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5cc6ca-70dc-4d19-8229-7ec381dd76fc",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "ef54d691-d4a3-4057-9428-27abe6105d88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "1a35696a-eb49-4d0c-b408-a48dbd0883b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef54d691-d4a3-4057-9428-27abe6105d88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ef4a47-1d81-49c4-9202-f2f2bad137a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef54d691-d4a3-4057-9428-27abe6105d88",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "1b32abc4-85a8-4d65-80a6-70157387cc10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "bd8cdb63-7604-4783-a695-1c4a2a36aaf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b32abc4-85a8-4d65-80a6-70157387cc10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "584f7af2-fa3c-4d1c-a38e-d0b5a9ad6c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b32abc4-85a8-4d65-80a6-70157387cc10",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "06bcbfd4-f4f6-4967-ad70-4f5fd76b63c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "03266980-7b4c-45f0-b556-01b694e1c367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06bcbfd4-f4f6-4967-ad70-4f5fd76b63c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e3fcf0-224a-43b9-a582-bca3a44e3f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06bcbfd4-f4f6-4967-ad70-4f5fd76b63c7",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "e3cb3d21-bce8-46ff-b6ca-4441c49fb8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "bb01119c-868f-41d0-89a2-b6f7c6c6bc48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3cb3d21-bce8-46ff-b6ca-4441c49fb8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc00c18-e478-4995-b5a0-d537ceb15855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3cb3d21-bce8-46ff-b6ca-4441c49fb8b7",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "91faddc1-3d4f-4f06-81c2-506f4d29bb0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "db91b9e0-188f-4927-966b-5cabe8cebfee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91faddc1-3d4f-4f06-81c2-506f4d29bb0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbf03d4f-d141-400c-8cf6-340765f43c13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91faddc1-3d4f-4f06-81c2-506f4d29bb0e",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        },
        {
            "id": "e32e0463-152f-4b9b-a2c2-a6cbe367dcc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "compositeImage": {
                "id": "96010193-1a82-4882-89e7-cb54baf33ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e32e0463-152f-4b9b-a2c2-a6cbe367dcc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4a4921b-e2cf-4890-9015-bc151904c477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e32e0463-152f-4b9b-a2c2-a6cbe367dcc2",
                    "LayerId": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "32fc5ba1-d9f2-4876-bc73-9ed92700ce90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5be9481-8a8f-4edd-9e9b-2d1dd37f2df0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}