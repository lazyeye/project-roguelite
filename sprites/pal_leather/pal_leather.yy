{
    "id": "a7892262-f3ed-409a-80ab-7817422f45dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pal_leather",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02ffab17-25d3-4e88-9540-1c77f4c96ba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7892262-f3ed-409a-80ab-7817422f45dc",
            "compositeImage": {
                "id": "d2b99bc5-5323-40b3-8103-6880dbd7ff3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ffab17-25d3-4e88-9540-1c77f4c96ba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1afbda1-c149-4bb4-abb0-2d45ca2e56f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ffab17-25d3-4e88-9540-1c77f4c96ba6",
                    "LayerId": "39257db3-4a49-4a82-b864-eaa0f853d65b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "39257db3-4a49-4a82-b864-eaa0f853d65b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7892262-f3ed-409a-80ab-7817422f45dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}