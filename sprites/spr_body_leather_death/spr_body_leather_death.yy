{
    "id": "e2d55a4b-56a9-4068-a4a5-5decf397743b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_leather_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a52a1a9b-9ae9-48d9-a8ba-4a4268c60710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d55a4b-56a9-4068-a4a5-5decf397743b",
            "compositeImage": {
                "id": "9cc10e5e-e2b4-46d2-929c-d5958de1ecb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a52a1a9b-9ae9-48d9-a8ba-4a4268c60710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c342e3d5-df4a-4780-9136-2dc2fbb234bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a52a1a9b-9ae9-48d9-a8ba-4a4268c60710",
                    "LayerId": "aff0df72-164f-4e9b-a050-0c590a1ba0f7"
                }
            ]
        },
        {
            "id": "ffce5cc6-98c3-4449-abb8-83041e7b6776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d55a4b-56a9-4068-a4a5-5decf397743b",
            "compositeImage": {
                "id": "eb74c585-0f91-4647-bbf8-ad9eef911c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffce5cc6-98c3-4449-abb8-83041e7b6776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d839a41-6669-48b9-96bc-bb61212559b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffce5cc6-98c3-4449-abb8-83041e7b6776",
                    "LayerId": "aff0df72-164f-4e9b-a050-0c590a1ba0f7"
                }
            ]
        },
        {
            "id": "6483e8a6-8910-448a-8f65-eb4dd3e0dd20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2d55a4b-56a9-4068-a4a5-5decf397743b",
            "compositeImage": {
                "id": "7ec069bf-d945-4a0b-a9dd-d04f5c36291e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6483e8a6-8910-448a-8f65-eb4dd3e0dd20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66a8bad-b7e2-4083-b461-a69d84aa210d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6483e8a6-8910-448a-8f65-eb4dd3e0dd20",
                    "LayerId": "aff0df72-164f-4e9b-a050-0c590a1ba0f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "aff0df72-164f-4e9b-a050-0c590a1ba0f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2d55a4b-56a9-4068-a4a5-5decf397743b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}