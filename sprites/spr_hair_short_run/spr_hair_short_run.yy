{
    "id": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_short_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34cfb20d-df56-4e0d-b782-0308957949fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "44c01795-64cb-44c2-aa9c-1fcb764110f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34cfb20d-df56-4e0d-b782-0308957949fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8131829c-e5d9-4129-87a3-287ab56fa359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34cfb20d-df56-4e0d-b782-0308957949fb",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "8e95d472-ef95-4852-97ea-d97c5f21c758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "2cad31e8-f494-407c-83fb-9f806d45ad5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e95d472-ef95-4852-97ea-d97c5f21c758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa7fb25-c510-4f18-a6de-3cf324a79c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e95d472-ef95-4852-97ea-d97c5f21c758",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "1a556e78-1344-498d-9d77-3f525fd666d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "d27cc9b9-9476-4666-a276-3a20eefd59ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a556e78-1344-498d-9d77-3f525fd666d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650322c6-ee22-40f2-a2bf-cab0515884be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a556e78-1344-498d-9d77-3f525fd666d0",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "ca6324cc-1b9d-4b58-9d06-1c647169cac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "15885c23-3bb9-4a8f-a095-4b885c29949c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6324cc-1b9d-4b58-9d06-1c647169cac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f6ad77e-0ae4-49b9-9c38-b306e3f6ee07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6324cc-1b9d-4b58-9d06-1c647169cac3",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "e43d8e67-e760-413e-9629-a3123cc60d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "6d75d505-15e0-4f8e-832b-17b00f363e11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e43d8e67-e760-413e-9629-a3123cc60d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f01f51-21b0-4394-8e3a-4d467b0d6664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e43d8e67-e760-413e-9629-a3123cc60d8e",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "abead592-9573-4518-815d-a2761471e8dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "5a9bf909-06ba-4458-bb0f-6ac84f32fe87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abead592-9573-4518-815d-a2761471e8dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ec77c4-f698-4d16-b4c7-087fb28b8aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abead592-9573-4518-815d-a2761471e8dd",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "eda70c12-6f2c-45a4-bac2-554690c3a400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "3e434b19-e739-4dcd-955c-2dc5f738d235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda70c12-6f2c-45a4-bac2-554690c3a400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d39db28-70e4-49ff-b14b-7a47811c8ba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda70c12-6f2c-45a4-bac2-554690c3a400",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "62e2e60e-0f32-4f4f-b73c-0c0089ff6a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "43a623cf-7593-4fd6-8da9-7a662ed163c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e2e60e-0f32-4f4f-b73c-0c0089ff6a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "754857ac-a94e-4051-b2cc-cb2875d9019c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e2e60e-0f32-4f4f-b73c-0c0089ff6a69",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "b7c3bbfb-f68a-4000-a495-5b36b96428cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "fe2c270d-4eef-4106-b228-4329b7c3e111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c3bbfb-f68a-4000-a495-5b36b96428cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21678eb0-d159-47ff-a953-c9393cffaff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c3bbfb-f68a-4000-a495-5b36b96428cb",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "8d318a14-668a-4047-852a-0f021560ad6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "9341b03c-f99b-43b0-ac41-7a3005cf7447",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d318a14-668a-4047-852a-0f021560ad6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb041d81-af88-4bba-8121-f7025db0c0c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d318a14-668a-4047-852a-0f021560ad6e",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "d576a446-c527-4db7-b143-4db7c6f5d5ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "3a7859dc-2724-4182-acaa-84b17bd5c4f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d576a446-c527-4db7-b143-4db7c6f5d5ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3addc12-0abf-43ad-bf62-07fab654072f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d576a446-c527-4db7-b143-4db7c6f5d5ab",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "4188abf0-924f-4447-bea3-c6a1ced90ba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "1d339b99-6f26-45ed-b5c0-58fa7bea57e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4188abf0-924f-4447-bea3-c6a1ced90ba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51bf22a5-a969-4340-899a-fd1cce77094e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4188abf0-924f-4447-bea3-c6a1ced90ba9",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "2d296b6a-0147-4ae7-8d06-cec113951fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "146896ae-2ffb-4d0a-8e53-926dd27e4287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d296b6a-0147-4ae7-8d06-cec113951fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6956e82-f44b-4477-ad39-360dd1ca283c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d296b6a-0147-4ae7-8d06-cec113951fa0",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "862c1969-36da-4f9a-bab9-0d2b843aeded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "df163864-8172-4e99-b135-c77ae10f3956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "862c1969-36da-4f9a-bab9-0d2b843aeded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909adcf1-7b9b-4d0f-940f-a4641ce752d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "862c1969-36da-4f9a-bab9-0d2b843aeded",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "b7d44bc6-f2e7-48e2-94b1-fce4fdb3ecf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "d5d4ee25-21ec-4fb1-a6c3-c950c899277e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d44bc6-f2e7-48e2-94b1-fce4fdb3ecf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb87b327-6514-414a-a08d-02a3f3e9d49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d44bc6-f2e7-48e2-94b1-fce4fdb3ecf0",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        },
        {
            "id": "1f93b24f-1a3a-4595-a660-c63bf92d5bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "compositeImage": {
                "id": "2f5c287f-bfcc-425b-a433-6f9ea97a580d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f93b24f-1a3a-4595-a660-c63bf92d5bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5033d94a-9eb1-42e6-bd11-dd0b80087aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f93b24f-1a3a-4595-a660-c63bf92d5bb7",
                    "LayerId": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "3a9f5758-da98-4b73-89e5-2bfb8b593ad9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3be2214e-fd0c-4549-8c34-cb7dfae0324c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}