{
    "id": "cb2e275a-bd3f-4b68-a0a1-e49b3d732e7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54624e94-4c52-42ef-9ae7-86236be39dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb2e275a-bd3f-4b68-a0a1-e49b3d732e7f",
            "compositeImage": {
                "id": "77f70d6d-dd31-49aa-aa8a-b28198344562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54624e94-4c52-42ef-9ae7-86236be39dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0780360d-df46-4a3c-8950-5f9f36ff4a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54624e94-4c52-42ef-9ae7-86236be39dc2",
                    "LayerId": "655d5be5-3690-4357-9a5b-335066e2b8ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "655d5be5-3690-4357-9a5b-335066e2b8ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb2e275a-bd3f-4b68-a0a1-e49b3d732e7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}