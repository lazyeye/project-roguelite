{
    "id": "506721a8-5262-4b35-9d05-576477545edf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 5,
    "bbox_right": 8,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "031f5b49-5d97-4cdf-ba80-f5a623e17f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "506721a8-5262-4b35-9d05-576477545edf",
            "compositeImage": {
                "id": "c0ca29b6-5025-4766-ab02-21057115091e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "031f5b49-5d97-4cdf-ba80-f5a623e17f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b88e4ca-7c1a-45bd-9b71-732fd9160bbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "031f5b49-5d97-4cdf-ba80-f5a623e17f7b",
                    "LayerId": "752da570-b726-467e-b400-6566eaa2b2d2"
                }
            ]
        },
        {
            "id": "9944c0b2-d8fc-41c9-8bec-319b4f3f6139",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "506721a8-5262-4b35-9d05-576477545edf",
            "compositeImage": {
                "id": "669da24b-4b11-4690-b290-3537fabf32d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9944c0b2-d8fc-41c9-8bec-319b4f3f6139",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890fca83-f7e2-468d-99b7-e1ea42697c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9944c0b2-d8fc-41c9-8bec-319b4f3f6139",
                    "LayerId": "752da570-b726-467e-b400-6566eaa2b2d2"
                }
            ]
        },
        {
            "id": "28c65db6-4019-46e5-8ac3-81a792988706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "506721a8-5262-4b35-9d05-576477545edf",
            "compositeImage": {
                "id": "0501d3ba-a0e2-4e38-a225-6f602c295ebe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28c65db6-4019-46e5-8ac3-81a792988706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408d0ea0-e7a5-4e22-89ee-8a094c735e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28c65db6-4019-46e5-8ac3-81a792988706",
                    "LayerId": "752da570-b726-467e-b400-6566eaa2b2d2"
                }
            ]
        },
        {
            "id": "5672eef7-8c36-4059-9dbc-219ff87990ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "506721a8-5262-4b35-9d05-576477545edf",
            "compositeImage": {
                "id": "47c6ddf5-b7e1-4fe3-8f08-7b479c33faf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5672eef7-8c36-4059-9dbc-219ff87990ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97f27ab-c7ac-4b59-b6b8-80a56e067244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5672eef7-8c36-4059-9dbc-219ff87990ee",
                    "LayerId": "752da570-b726-467e-b400-6566eaa2b2d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "752da570-b726-467e-b400-6566eaa2b2d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "506721a8-5262-4b35-9d05-576477545edf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}