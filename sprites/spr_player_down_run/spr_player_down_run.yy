{
    "id": "2d7b976c-93c1-4363-979d-585a58941329",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1a693b5-d184-4ba9-93ae-2216fdc9f0ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d7b976c-93c1-4363-979d-585a58941329",
            "compositeImage": {
                "id": "5e3e066f-ebf4-4434-b1c8-8e5205e4c4e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1a693b5-d184-4ba9-93ae-2216fdc9f0ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b7309c1-bfff-41e7-b009-7ec023dc34a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1a693b5-d184-4ba9-93ae-2216fdc9f0ed",
                    "LayerId": "8733fe7a-cf7b-4369-966e-7a70d02de243"
                }
            ]
        },
        {
            "id": "960ce152-26a3-4146-9fa5-21bfa03ccc7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d7b976c-93c1-4363-979d-585a58941329",
            "compositeImage": {
                "id": "a6b0969f-df85-4218-a591-4b6899552229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960ce152-26a3-4146-9fa5-21bfa03ccc7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55cba30-9303-4849-8b0d-d988b4550c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960ce152-26a3-4146-9fa5-21bfa03ccc7c",
                    "LayerId": "8733fe7a-cf7b-4369-966e-7a70d02de243"
                }
            ]
        },
        {
            "id": "85d4ed87-91b3-4be4-8711-92a2ca32c3f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d7b976c-93c1-4363-979d-585a58941329",
            "compositeImage": {
                "id": "24ae3528-a677-4d3c-b386-a29d1738e130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d4ed87-91b3-4be4-8711-92a2ca32c3f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd19c441-48d4-49c1-8dbe-d9654b55c7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d4ed87-91b3-4be4-8711-92a2ca32c3f0",
                    "LayerId": "8733fe7a-cf7b-4369-966e-7a70d02de243"
                }
            ]
        },
        {
            "id": "91735218-b876-4393-9719-20681608cd5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d7b976c-93c1-4363-979d-585a58941329",
            "compositeImage": {
                "id": "10ee4ad0-7a3c-44de-9d3a-1e1bca75fde2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91735218-b876-4393-9719-20681608cd5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e5177d-d43c-4ee7-8cc7-71e2c9b0f17d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91735218-b876-4393-9719-20681608cd5b",
                    "LayerId": "8733fe7a-cf7b-4369-966e-7a70d02de243"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "8733fe7a-cf7b-4369-966e-7a70d02de243",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d7b976c-93c1-4363-979d-585a58941329",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}