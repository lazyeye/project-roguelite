{
    "id": "8fb7eacb-6dde-439a-a712-f96c742672db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_short_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 5,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89992532-379f-41a7-a152-8db05ea8b15e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb7eacb-6dde-439a-a712-f96c742672db",
            "compositeImage": {
                "id": "92a095a0-182a-4105-80c9-2169c34c0a6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89992532-379f-41a7-a152-8db05ea8b15e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0501f5ef-1b9d-4f42-a058-47be37f44344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89992532-379f-41a7-a152-8db05ea8b15e",
                    "LayerId": "1e3c0e0c-1a8b-41dd-8e4a-ab6613e59408"
                }
            ]
        },
        {
            "id": "53aa7e2f-8205-4b85-ba90-2ac423f8153e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb7eacb-6dde-439a-a712-f96c742672db",
            "compositeImage": {
                "id": "dff7f56d-3f6a-48d1-96ea-319ec79f7753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53aa7e2f-8205-4b85-ba90-2ac423f8153e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e6682f1-df34-4e07-9c5c-edbb7be054db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53aa7e2f-8205-4b85-ba90-2ac423f8153e",
                    "LayerId": "1e3c0e0c-1a8b-41dd-8e4a-ab6613e59408"
                }
            ]
        },
        {
            "id": "a6226821-b6ad-4df7-a971-56dc990a67ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fb7eacb-6dde-439a-a712-f96c742672db",
            "compositeImage": {
                "id": "cf332fc4-f7f8-4521-ae8a-5b7130dbcfc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6226821-b6ad-4df7-a971-56dc990a67ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a7477e-688d-45b4-891b-c14b2df58d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6226821-b6ad-4df7-a971-56dc990a67ef",
                    "LayerId": "1e3c0e0c-1a8b-41dd-8e4a-ab6613e59408"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "1e3c0e0c-1a8b-41dd-8e4a-ab6613e59408",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fb7eacb-6dde-439a-a712-f96c742672db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}