{
    "id": "8c152d23-8305-4945-8a5b-e0aae0df385a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_titan_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a849abe6-7193-49d0-ab1b-d03db9e4fcb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c152d23-8305-4945-8a5b-e0aae0df385a",
            "compositeImage": {
                "id": "608ac93b-0f8a-4184-9699-b4cbf31ccdad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a849abe6-7193-49d0-ab1b-d03db9e4fcb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bdf17d3-03f0-473a-bb3d-7353af517025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a849abe6-7193-49d0-ab1b-d03db9e4fcb1",
                    "LayerId": "6d3b9db5-e138-45f4-9503-64bc3ddb9e93"
                }
            ]
        },
        {
            "id": "f1ad17b1-b4ab-4547-a983-c04e5a7af7bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c152d23-8305-4945-8a5b-e0aae0df385a",
            "compositeImage": {
                "id": "c223d198-488d-4edf-bd9d-b95227f2d214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ad17b1-b4ab-4547-a983-c04e5a7af7bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774aa43e-bf86-40fc-a2f6-13b3b4bbb2c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ad17b1-b4ab-4547-a983-c04e5a7af7bd",
                    "LayerId": "6d3b9db5-e138-45f4-9503-64bc3ddb9e93"
                }
            ]
        },
        {
            "id": "4ba90a79-8a19-4f76-a526-85b11195984c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c152d23-8305-4945-8a5b-e0aae0df385a",
            "compositeImage": {
                "id": "080d4173-9c18-4a94-861c-e23208ff3b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba90a79-8a19-4f76-a526-85b11195984c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce390e9-f0e0-46b6-8594-547ea8203128",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba90a79-8a19-4f76-a526-85b11195984c",
                    "LayerId": "6d3b9db5-e138-45f4-9503-64bc3ddb9e93"
                }
            ]
        },
        {
            "id": "c8158b2e-8460-4702-af0b-01cc39931c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c152d23-8305-4945-8a5b-e0aae0df385a",
            "compositeImage": {
                "id": "5188c33d-640e-4f3a-be4e-d6653ffa5f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8158b2e-8460-4702-af0b-01cc39931c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0ace26-1bbb-4ced-b628-2398e9016a06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8158b2e-8460-4702-af0b-01cc39931c0f",
                    "LayerId": "6d3b9db5-e138-45f4-9503-64bc3ddb9e93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "6d3b9db5-e138-45f4-9503-64bc3ddb9e93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c152d23-8305-4945-8a5b-e0aae0df385a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}