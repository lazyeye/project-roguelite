{
    "id": "49813135-7bc2-460c-8cb8-1fd94008009b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_leather_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db704b82-840d-4f5e-9d30-f51ecc84e7fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49813135-7bc2-460c-8cb8-1fd94008009b",
            "compositeImage": {
                "id": "3fb445b2-300e-4aa0-b4e7-70e5b2173a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db704b82-840d-4f5e-9d30-f51ecc84e7fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7bc50aa-1c04-4094-9d49-859667eea10c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db704b82-840d-4f5e-9d30-f51ecc84e7fc",
                    "LayerId": "1d8ba968-6596-4e55-ab6c-0acc29790057"
                }
            ]
        },
        {
            "id": "a3e0dcd1-1e4a-4616-bc97-ddbf0ceb77f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49813135-7bc2-460c-8cb8-1fd94008009b",
            "compositeImage": {
                "id": "60e2fc1a-aef2-43dc-be5f-95f98c99e1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e0dcd1-1e4a-4616-bc97-ddbf0ceb77f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd988deb-ef80-41f2-9c4b-fb88e1770715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e0dcd1-1e4a-4616-bc97-ddbf0ceb77f2",
                    "LayerId": "1d8ba968-6596-4e55-ab6c-0acc29790057"
                }
            ]
        },
        {
            "id": "dd29a727-d73b-45c3-a182-98a37361f2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49813135-7bc2-460c-8cb8-1fd94008009b",
            "compositeImage": {
                "id": "143ea269-42d2-4869-a74c-b3bb6d3bfecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd29a727-d73b-45c3-a182-98a37361f2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc088ec-9ca2-44bf-8f0a-786420a908dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd29a727-d73b-45c3-a182-98a37361f2d9",
                    "LayerId": "1d8ba968-6596-4e55-ab6c-0acc29790057"
                }
            ]
        },
        {
            "id": "526df9c8-5060-4742-bf0a-c78a8bafcc78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49813135-7bc2-460c-8cb8-1fd94008009b",
            "compositeImage": {
                "id": "9ab01278-3520-4a56-9a17-f3c4f6be61df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526df9c8-5060-4742-bf0a-c78a8bafcc78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691ff351-cb31-40c2-b964-b488742bd9bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526df9c8-5060-4742-bf0a-c78a8bafcc78",
                    "LayerId": "1d8ba968-6596-4e55-ab6c-0acc29790057"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "1d8ba968-6596-4e55-ab6c-0acc29790057",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49813135-7bc2-460c-8cb8-1fd94008009b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}