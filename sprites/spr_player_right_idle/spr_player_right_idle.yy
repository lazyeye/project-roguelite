{
    "id": "7aeba818-278e-401e-b0e3-f501fc6d0441",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e9f9670-9028-4e07-a18b-78c277410ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aeba818-278e-401e-b0e3-f501fc6d0441",
            "compositeImage": {
                "id": "77b33cca-c9ad-4936-8d05-ac9ef5cbc5f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9f9670-9028-4e07-a18b-78c277410ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6225af-3160-40ca-9061-3b5b314e6f52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9f9670-9028-4e07-a18b-78c277410ee8",
                    "LayerId": "9180a6da-bc01-469e-9da3-b4999cc63cba"
                }
            ]
        },
        {
            "id": "58872def-208f-4d61-885c-09282ad96f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7aeba818-278e-401e-b0e3-f501fc6d0441",
            "compositeImage": {
                "id": "e68ed4ce-c986-4b1f-aa68-dab663be4bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58872def-208f-4d61-885c-09282ad96f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9c651d3-2b92-4844-a969-067f2244fae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58872def-208f-4d61-885c-09282ad96f84",
                    "LayerId": "9180a6da-bc01-469e-9da3-b4999cc63cba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "9180a6da-bc01-469e-9da3-b4999cc63cba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7aeba818-278e-401e-b0e3-f501fc6d0441",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}