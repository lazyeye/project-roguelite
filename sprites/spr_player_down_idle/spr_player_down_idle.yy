{
    "id": "1dc12aac-818e-4e83-9cb7-a63e96d22da2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9698ae89-b9d3-40a7-8add-5c0e9433c2cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dc12aac-818e-4e83-9cb7-a63e96d22da2",
            "compositeImage": {
                "id": "370a1e67-ca0b-4d49-9c26-0478c2a5f4b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9698ae89-b9d3-40a7-8add-5c0e9433c2cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e8e118-3960-454c-a10b-0a60aef364c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9698ae89-b9d3-40a7-8add-5c0e9433c2cf",
                    "LayerId": "73dc94fb-cc06-48e4-9583-e4c79ad79a52"
                }
            ]
        },
        {
            "id": "05e32f80-36a3-4257-b8bf-bae5b40ed367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dc12aac-818e-4e83-9cb7-a63e96d22da2",
            "compositeImage": {
                "id": "770a1b0b-2da1-44b1-8097-aefff7fec633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e32f80-36a3-4257-b8bf-bae5b40ed367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450fe8cc-5bb6-44ad-9252-4384fbc6ee44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e32f80-36a3-4257-b8bf-bae5b40ed367",
                    "LayerId": "73dc94fb-cc06-48e4-9583-e4c79ad79a52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "73dc94fb-cc06-48e4-9583-e4c79ad79a52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dc12aac-818e-4e83-9cb7-a63e96d22da2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}