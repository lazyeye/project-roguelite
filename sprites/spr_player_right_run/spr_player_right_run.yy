{
    "id": "bedc2901-c0c5-4081-b384-9c4266101646",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e368d74-3ebd-430e-ba50-0d10215a79ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bedc2901-c0c5-4081-b384-9c4266101646",
            "compositeImage": {
                "id": "ae5e6804-c1fd-412b-8bf3-996b66c0f5d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e368d74-3ebd-430e-ba50-0d10215a79ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2f4451-5350-4290-9cd9-8a9bd4d9376c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e368d74-3ebd-430e-ba50-0d10215a79ce",
                    "LayerId": "d9fb99f5-7932-479f-afad-2e8ba57ca8b4"
                }
            ]
        },
        {
            "id": "b42336c0-1546-4935-b626-99222077a011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bedc2901-c0c5-4081-b384-9c4266101646",
            "compositeImage": {
                "id": "4e5e2edf-7dc8-4b36-bfef-60b94619636f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b42336c0-1546-4935-b626-99222077a011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c905ab68-2969-4908-93e5-21a24effbbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b42336c0-1546-4935-b626-99222077a011",
                    "LayerId": "d9fb99f5-7932-479f-afad-2e8ba57ca8b4"
                }
            ]
        },
        {
            "id": "44c10950-c9fd-401c-b453-b9d112b62a43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bedc2901-c0c5-4081-b384-9c4266101646",
            "compositeImage": {
                "id": "18f834e2-bfe8-478a-a9d8-123776880dbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44c10950-c9fd-401c-b453-b9d112b62a43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2e60c4-b3de-4d03-8c7e-9ab203098053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44c10950-c9fd-401c-b453-b9d112b62a43",
                    "LayerId": "d9fb99f5-7932-479f-afad-2e8ba57ca8b4"
                }
            ]
        },
        {
            "id": "2cb4d432-fe88-44d8-9cc3-f50aa3cf4dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bedc2901-c0c5-4081-b384-9c4266101646",
            "compositeImage": {
                "id": "e46191c7-5503-40c6-9c44-c60cbade32b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cb4d432-fe88-44d8-9cc3-f50aa3cf4dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d45072f-0888-4e37-bf3f-2299fef0f6a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cb4d432-fe88-44d8-9cc3-f50aa3cf4dba",
                    "LayerId": "d9fb99f5-7932-479f-afad-2e8ba57ca8b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "d9fb99f5-7932-479f-afad-2e8ba57ca8b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bedc2901-c0c5-4081-b384-9c4266101646",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}