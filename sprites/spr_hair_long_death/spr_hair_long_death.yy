{
    "id": "57d77098-63d0-4ad6-8bd8-bb1bce97a5d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_long_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88a9d0ed-6f1b-4f72-973c-376d1401eccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d77098-63d0-4ad6-8bd8-bb1bce97a5d1",
            "compositeImage": {
                "id": "e2f3a173-1e71-4072-9d01-6022f66d0c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a9d0ed-6f1b-4f72-973c-376d1401eccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2410f5a0-5935-4086-8d37-e632f09390d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a9d0ed-6f1b-4f72-973c-376d1401eccf",
                    "LayerId": "69abb486-8bf3-4a87-a71d-78f6809ea840"
                }
            ]
        },
        {
            "id": "5dab4cbf-455c-408d-a65a-5ecc8a79907c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d77098-63d0-4ad6-8bd8-bb1bce97a5d1",
            "compositeImage": {
                "id": "5773fdaf-319f-4487-8afe-19bc8a830bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dab4cbf-455c-408d-a65a-5ecc8a79907c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba00250-9f54-4156-ad10-2a0441193232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dab4cbf-455c-408d-a65a-5ecc8a79907c",
                    "LayerId": "69abb486-8bf3-4a87-a71d-78f6809ea840"
                }
            ]
        },
        {
            "id": "b6f3ad0d-14c4-4026-af5a-9d2354012448",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d77098-63d0-4ad6-8bd8-bb1bce97a5d1",
            "compositeImage": {
                "id": "1fa2b10b-d2a5-46fd-aa8b-47d27e2df52d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6f3ad0d-14c4-4026-af5a-9d2354012448",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8494eeb-9891-496c-bf86-b02ff3faf7da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6f3ad0d-14c4-4026-af5a-9d2354012448",
                    "LayerId": "69abb486-8bf3-4a87-a71d-78f6809ea840"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "69abb486-8bf3-4a87-a71d-78f6809ea840",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57d77098-63d0-4ad6-8bd8-bb1bce97a5d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}