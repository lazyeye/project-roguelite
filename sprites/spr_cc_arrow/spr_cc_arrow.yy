{
    "id": "bbc9bdbc-1778-42dd-838b-005243e766da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cc_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a6ff03d-d341-4a85-8e48-4d99210abcc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc9bdbc-1778-42dd-838b-005243e766da",
            "compositeImage": {
                "id": "1d9d4eef-9f5b-497d-b83e-aff9e32b8d1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a6ff03d-d341-4a85-8e48-4d99210abcc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a8c6ecc-20e5-4544-b98a-41242f15b296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a6ff03d-d341-4a85-8e48-4d99210abcc7",
                    "LayerId": "27ea5ad8-905b-4391-b9a8-c4d87421aca1"
                }
            ]
        },
        {
            "id": "6738e613-1cde-478e-b584-a0aa71cf2b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc9bdbc-1778-42dd-838b-005243e766da",
            "compositeImage": {
                "id": "ebf3cebb-5540-4999-a73d-4f8c99026497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6738e613-1cde-478e-b584-a0aa71cf2b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051897d6-eeae-42c8-a1b9-d366293ea63b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6738e613-1cde-478e-b584-a0aa71cf2b83",
                    "LayerId": "27ea5ad8-905b-4391-b9a8-c4d87421aca1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "27ea5ad8-905b-4391-b9a8-c4d87421aca1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbc9bdbc-1778-42dd-838b-005243e766da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 0,
    "yorig": 0
}