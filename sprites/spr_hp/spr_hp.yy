{
    "id": "91a86f13-cf15-427d-b173-ea060d1da408",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87f39e72-1b89-43c5-9bdc-5506a79ccc1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a86f13-cf15-427d-b173-ea060d1da408",
            "compositeImage": {
                "id": "5c014579-fd29-4778-943f-1537796edfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87f39e72-1b89-43c5-9bdc-5506a79ccc1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d24bba52-c7ee-4b58-b19f-807d877b209f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87f39e72-1b89-43c5-9bdc-5506a79ccc1d",
                    "LayerId": "ae0f8741-b935-4dd4-beff-ce815b287d8e"
                }
            ]
        },
        {
            "id": "157a8b6e-3583-4db7-b5d2-f8311b95478e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a86f13-cf15-427d-b173-ea060d1da408",
            "compositeImage": {
                "id": "bfdc2db8-45cc-4d96-a789-68c0b351371d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "157a8b6e-3583-4db7-b5d2-f8311b95478e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5f3e9e-bb9a-4045-8f67-c1da6d98e4d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "157a8b6e-3583-4db7-b5d2-f8311b95478e",
                    "LayerId": "ae0f8741-b935-4dd4-beff-ce815b287d8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "ae0f8741-b935-4dd4-beff-ce815b287d8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91a86f13-cf15-427d-b173-ea060d1da408",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}