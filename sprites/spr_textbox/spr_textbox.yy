{
    "id": "8f1b346c-df39-4d97-a4aa-564eb0dba92e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9903cdb-abc2-4c38-bfa5-406fd14bf246",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f1b346c-df39-4d97-a4aa-564eb0dba92e",
            "compositeImage": {
                "id": "1678b918-b6fe-46cf-b7c7-931f97614633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9903cdb-abc2-4c38-bfa5-406fd14bf246",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc44e38-de1e-4d2e-b4d5-27cfec88dd3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9903cdb-abc2-4c38-bfa5-406fd14bf246",
                    "LayerId": "0d12396b-912c-472c-94af-cbad07a72964"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0d12396b-912c-472c-94af-cbad07a72964",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f1b346c-df39-4d97-a4aa-564eb0dba92e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}