{
    "id": "2bd2dabe-6a35-4315-b9d6-54fcd5dd95b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29eb0a89-257d-4aff-98d8-9f2faa257d05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd2dabe-6a35-4315-b9d6-54fcd5dd95b1",
            "compositeImage": {
                "id": "52caf85f-2e41-4a06-99e7-b9d5807d8dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29eb0a89-257d-4aff-98d8-9f2faa257d05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c8a5be5-949a-451a-9af8-69f22ab9127a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29eb0a89-257d-4aff-98d8-9f2faa257d05",
                    "LayerId": "d83fba2a-de87-4784-ac73-e8dec12568df"
                }
            ]
        },
        {
            "id": "986361ba-261a-4d87-8ff9-ecca8b39ae04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd2dabe-6a35-4315-b9d6-54fcd5dd95b1",
            "compositeImage": {
                "id": "4c068c7b-590c-44f9-aa9a-ab9954b41fdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "986361ba-261a-4d87-8ff9-ecca8b39ae04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23c75b3c-fff0-4f20-8462-8bfe1f820205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "986361ba-261a-4d87-8ff9-ecca8b39ae04",
                    "LayerId": "d83fba2a-de87-4784-ac73-e8dec12568df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "d83fba2a-de87-4784-ac73-e8dec12568df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bd2dabe-6a35-4315-b9d6-54fcd5dd95b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}