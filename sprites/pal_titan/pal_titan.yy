{
    "id": "5039c0b9-31cf-437c-91c8-8a6de19902a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pal_titan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdc35d28-db3e-4edf-912b-b103ac917393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5039c0b9-31cf-437c-91c8-8a6de19902a2",
            "compositeImage": {
                "id": "5def7f1e-830a-49b1-a61c-5d0177413a1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdc35d28-db3e-4edf-912b-b103ac917393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0031332a-8d01-4fd3-ad87-c906f44654a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdc35d28-db3e-4edf-912b-b103ac917393",
                    "LayerId": "cbf8d53e-090f-4200-9473-9911a1fd0b73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "cbf8d53e-090f-4200-9473-9911a1fd0b73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5039c0b9-31cf-437c-91c8-8a6de19902a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}