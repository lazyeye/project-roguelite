{
    "id": "3a7294d3-bd75-46c4-9954-971292760e73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_spiky_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8e9b61c-66a3-4553-af11-52898e8815a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "9fbc0ecd-2068-4ccb-b0ae-7a277a70cd1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e9b61c-66a3-4553-af11-52898e8815a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a60a98-c633-46a9-8988-72ac72d2d147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e9b61c-66a3-4553-af11-52898e8815a6",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "a1ad7547-6841-4d5a-91d6-4265235c1e23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "adf83d1f-b404-49bb-8783-07e93e620b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1ad7547-6841-4d5a-91d6-4265235c1e23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd82d695-f68e-4f85-b67b-d9a8bcf83ddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1ad7547-6841-4d5a-91d6-4265235c1e23",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "b167aea8-724a-4a52-8715-b9e8df8dd579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "37501eac-1dbf-4d0c-9d11-af7fcc39fa0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b167aea8-724a-4a52-8715-b9e8df8dd579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7480367-dbf9-4a3a-bb37-46c9de06ef85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b167aea8-724a-4a52-8715-b9e8df8dd579",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "752a1ce1-c441-467f-8d2b-f358da7ea15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "37bf9052-6be0-4bb0-a677-0ab21eb153f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "752a1ce1-c441-467f-8d2b-f358da7ea15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ddb3441-acf3-4e2d-8561-f4b8755d073a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "752a1ce1-c441-467f-8d2b-f358da7ea15f",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "0ae18d3d-f6bb-4333-9c09-9d4cf9289e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "662abbad-fe99-4d5f-8bfc-ad387deb77b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae18d3d-f6bb-4333-9c09-9d4cf9289e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc8bffdd-3c5c-446a-aaeb-10a80c953e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae18d3d-f6bb-4333-9c09-9d4cf9289e97",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "dc4a6c15-0076-4bc6-a2dd-9c9fbb90245b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "03fdd31d-ab55-402f-b1f9-372fe5e6167a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4a6c15-0076-4bc6-a2dd-9c9fbb90245b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7aaa58a-f5c3-42a4-a36c-029dd6796cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4a6c15-0076-4bc6-a2dd-9c9fbb90245b",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "41dffba3-72c7-45ed-a7b4-aa84e1498575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "1bb1bd91-eb86-4e42-be32-745c9939d14e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41dffba3-72c7-45ed-a7b4-aa84e1498575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e892ae-d2cf-4324-8441-bedf8bf662ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41dffba3-72c7-45ed-a7b4-aa84e1498575",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "b1908f88-7c7e-4ab7-8e69-5cc43b8df0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "8098ebac-3f02-4990-821a-9a945f92582f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1908f88-7c7e-4ab7-8e69-5cc43b8df0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a1214b6-2d7f-464c-99e8-277b3660f971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1908f88-7c7e-4ab7-8e69-5cc43b8df0bf",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "33a00590-88e5-4c8f-9f74-7fad94388841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "09804b1a-7c33-4411-9d0d-47b3c8182ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a00590-88e5-4c8f-9f74-7fad94388841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "193c1ce0-391a-4a44-9285-8bda8adb9265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a00590-88e5-4c8f-9f74-7fad94388841",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "d7c8980a-e98f-4f73-953d-1522c9b7f006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "3afff24e-3f34-4198-b4bf-2237126e150e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c8980a-e98f-4f73-953d-1522c9b7f006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c8154a-a5c9-46f9-bb8a-186e74cc3354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c8980a-e98f-4f73-953d-1522c9b7f006",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "567fa948-4c78-4b4c-85ea-1ebfde9f1866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "7ca2cbae-1bc4-41ac-a588-c9076b08648d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "567fa948-4c78-4b4c-85ea-1ebfde9f1866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88f0f97e-3d43-4bd4-b986-6365a6458e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "567fa948-4c78-4b4c-85ea-1ebfde9f1866",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "ee698475-4c54-4c29-a1fa-acca60da9629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "2027c44c-f9b2-4f73-bab7-fa686f0ee7cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee698475-4c54-4c29-a1fa-acca60da9629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08dd0d9-0a45-405a-ab97-e024a77935a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee698475-4c54-4c29-a1fa-acca60da9629",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "9e7d608c-fdfe-42df-9904-5ce679e4648d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "a313a098-ed4e-414d-811e-f7f987f60108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e7d608c-fdfe-42df-9904-5ce679e4648d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9482d80b-d430-45c5-85f2-60d2751284a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e7d608c-fdfe-42df-9904-5ce679e4648d",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "5c936224-dc4b-49ab-85cb-26f679fc256f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "bc8ee33d-1bb0-4e9e-b9de-8e35f13b8728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c936224-dc4b-49ab-85cb-26f679fc256f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "524e3b64-d44e-4651-910f-6a347904531a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c936224-dc4b-49ab-85cb-26f679fc256f",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "5fd0acdf-c4be-49f7-88c6-4bdded85ec0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "0b6784dc-6f1e-46ee-a9bd-bed50c1b3e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd0acdf-c4be-49f7-88c6-4bdded85ec0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c94ebdf-1a3f-4b61-bdee-535a9cde40c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd0acdf-c4be-49f7-88c6-4bdded85ec0a",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        },
        {
            "id": "d4dc99b1-3027-4490-80cb-90e54c0f3596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "compositeImage": {
                "id": "323863a0-5c59-4b45-a527-78b4e5eef545",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4dc99b1-3027-4490-80cb-90e54c0f3596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43c80bce-de50-47de-92a5-829e25d40fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4dc99b1-3027-4490-80cb-90e54c0f3596",
                    "LayerId": "905514af-e594-4d6a-96b2-46b577a888ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "905514af-e594-4d6a-96b2-46b577a888ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a7294d3-bd75-46c4-9954-971292760e73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}