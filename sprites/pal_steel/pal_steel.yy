{
    "id": "64fc3fa4-2d5a-46ba-ba77-abdb2247be13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pal_steel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f98a9cd1-4b75-408f-8c27-22bf10439b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64fc3fa4-2d5a-46ba-ba77-abdb2247be13",
            "compositeImage": {
                "id": "4063f966-3f56-4c72-932c-ebd4c061d260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98a9cd1-4b75-408f-8c27-22bf10439b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ec6b1a5-3809-4d5b-b5ef-7ed24247b7d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98a9cd1-4b75-408f-8c27-22bf10439b09",
                    "LayerId": "8c34c2c1-71ff-4cd0-aab7-9262639ae871"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "8c34c2c1-71ff-4cd0-aab7-9262639ae871",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64fc3fa4-2d5a-46ba-ba77-abdb2247be13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 0,
    "yorig": 0
}