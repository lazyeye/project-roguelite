{
    "id": "67387d10-e06b-4d03-839d-060e9e890d19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pal_hair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d67a147-0f33-4376-b96d-9261a8100e4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67387d10-e06b-4d03-839d-060e9e890d19",
            "compositeImage": {
                "id": "f8822f74-f802-46eb-8af1-e612ecf6f98e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d67a147-0f33-4376-b96d-9261a8100e4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d3509c-68b2-415d-b86d-14190c44196c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d67a147-0f33-4376-b96d-9261a8100e4c",
                    "LayerId": "866676fb-9e2c-4dfd-b629-335b324f9dd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "866676fb-9e2c-4dfd-b629-335b324f9dd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67387d10-e06b-4d03-839d-060e9e890d19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}