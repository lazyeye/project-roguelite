{
    "id": "0764c8bd-c8e2-4b0a-8bb0-bea0dad4371b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_long_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 3,
    "bbox_right": 9,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3129adca-1637-41e3-9448-1b23dbfe0b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0764c8bd-c8e2-4b0a-8bb0-bea0dad4371b",
            "compositeImage": {
                "id": "3a558e28-c5b4-4a10-8ffd-dc207739caec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3129adca-1637-41e3-9448-1b23dbfe0b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c3e33ae-5a4e-4b1a-8df7-8c3dddf8e04e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3129adca-1637-41e3-9448-1b23dbfe0b5a",
                    "LayerId": "e7f498d2-f230-455a-9dc3-acd13a24ab1b"
                }
            ]
        },
        {
            "id": "22e9f37d-a2be-47a8-a909-4781fd516848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0764c8bd-c8e2-4b0a-8bb0-bea0dad4371b",
            "compositeImage": {
                "id": "7eacc971-51b5-44e9-88af-b7f59ffd6d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e9f37d-a2be-47a8-a909-4781fd516848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0179eaac-68a0-4745-ad8a-35199e688ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e9f37d-a2be-47a8-a909-4781fd516848",
                    "LayerId": "e7f498d2-f230-455a-9dc3-acd13a24ab1b"
                }
            ]
        },
        {
            "id": "5fca1067-e30b-4c8e-9836-a968c96045fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0764c8bd-c8e2-4b0a-8bb0-bea0dad4371b",
            "compositeImage": {
                "id": "172e8172-9654-4ba2-b642-ddad9b7a4dcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fca1067-e30b-4c8e-9836-a968c96045fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c4b78b-b8ce-4a39-9ac7-a8a9a907a296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fca1067-e30b-4c8e-9836-a968c96045fe",
                    "LayerId": "e7f498d2-f230-455a-9dc3-acd13a24ab1b"
                }
            ]
        },
        {
            "id": "d2ad9297-5611-4a30-80a6-62f1008f9a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0764c8bd-c8e2-4b0a-8bb0-bea0dad4371b",
            "compositeImage": {
                "id": "13b5de18-081d-4674-8187-e059cefeaa19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ad9297-5611-4a30-80a6-62f1008f9a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71946e20-6c5e-40d0-8940-03a0488064ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ad9297-5611-4a30-80a6-62f1008f9a6a",
                    "LayerId": "e7f498d2-f230-455a-9dc3-acd13a24ab1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "e7f498d2-f230-455a-9dc3-acd13a24ab1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0764c8bd-c8e2-4b0a-8bb0-bea0dad4371b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}