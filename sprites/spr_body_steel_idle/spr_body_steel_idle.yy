{
    "id": "94748791-373f-4666-9b76-5349f5f74bc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_steel_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e435e388-3012-418b-ad01-e043597c35f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94748791-373f-4666-9b76-5349f5f74bc0",
            "compositeImage": {
                "id": "84976e72-2da4-4ef3-9c01-d9755bfab7dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e435e388-3012-418b-ad01-e043597c35f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8e2858-1cd1-40e8-98ae-7f3134528f9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e435e388-3012-418b-ad01-e043597c35f3",
                    "LayerId": "83826569-5548-43b8-99f9-ddbd269c1ef0"
                }
            ]
        },
        {
            "id": "1790a82a-18f5-441b-b443-221725781597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94748791-373f-4666-9b76-5349f5f74bc0",
            "compositeImage": {
                "id": "3e8a3328-1afd-425a-9e03-b28e012b5833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1790a82a-18f5-441b-b443-221725781597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a597707f-40c7-4b12-a8d9-bb8d631e7561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1790a82a-18f5-441b-b443-221725781597",
                    "LayerId": "83826569-5548-43b8-99f9-ddbd269c1ef0"
                }
            ]
        },
        {
            "id": "d5650deb-7486-42da-a88e-9968fb063f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94748791-373f-4666-9b76-5349f5f74bc0",
            "compositeImage": {
                "id": "e801ef25-12fc-45da-8031-4e3cdfa723c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5650deb-7486-42da-a88e-9968fb063f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06932e45-4cea-4ff8-9325-60f253b9821d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5650deb-7486-42da-a88e-9968fb063f55",
                    "LayerId": "83826569-5548-43b8-99f9-ddbd269c1ef0"
                }
            ]
        },
        {
            "id": "1924dc0f-b1d4-4a3b-b1f4-de7993693ec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94748791-373f-4666-9b76-5349f5f74bc0",
            "compositeImage": {
                "id": "35fd1631-2805-42db-8144-72ae042d18bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1924dc0f-b1d4-4a3b-b1f4-de7993693ec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586e2d10-7b25-40b6-95c6-9a27b9821480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1924dc0f-b1d4-4a3b-b1f4-de7993693ec8",
                    "LayerId": "83826569-5548-43b8-99f9-ddbd269c1ef0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "83826569-5548-43b8-99f9-ddbd269c1ef0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94748791-373f-4666-9b76-5349f5f74bc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}