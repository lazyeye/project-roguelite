{
    "id": "dc844da1-7b77-4736-960b-0c9dd662e834",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rogue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3b36793-ba74-4d99-9186-f6496656f89c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc844da1-7b77-4736-960b-0c9dd662e834",
            "compositeImage": {
                "id": "ccaa5793-cf08-4b0b-bfcf-36b37929762f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b36793-ba74-4d99-9186-f6496656f89c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a53b5f-7325-46a3-a3d1-9fb7845182fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b36793-ba74-4d99-9186-f6496656f89c",
                    "LayerId": "b6a817f0-1be2-47c2-a0a5-93d7c22c58af"
                }
            ]
        },
        {
            "id": "03dfff90-56c7-40ec-a66a-6ae6c458fca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc844da1-7b77-4736-960b-0c9dd662e834",
            "compositeImage": {
                "id": "a8d6c360-517d-4b29-8c20-1de092e8e91f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03dfff90-56c7-40ec-a66a-6ae6c458fca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c34abe7-d509-4cc7-a273-dd71dc13c2f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03dfff90-56c7-40ec-a66a-6ae6c458fca9",
                    "LayerId": "b6a817f0-1be2-47c2-a0a5-93d7c22c58af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b6a817f0-1be2-47c2-a0a5-93d7c22c58af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc844da1-7b77-4736-960b-0c9dd662e834",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}