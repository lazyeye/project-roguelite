{
    "id": "e372567f-1ab9-4466-851e-e9d6f890f6e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_titan_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23b6609c-8883-4cb1-9d70-ead6f3c95a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e372567f-1ab9-4466-851e-e9d6f890f6e4",
            "compositeImage": {
                "id": "5d47ac63-a47a-455b-bbe5-7de24ed98f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b6609c-8883-4cb1-9d70-ead6f3c95a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "534f5943-10f3-48c9-900e-1d7bfa40c95c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b6609c-8883-4cb1-9d70-ead6f3c95a00",
                    "LayerId": "c5bbd9bb-093b-4f96-9d75-578fc4ee1d3c"
                }
            ]
        },
        {
            "id": "302ee3f5-fe38-4c4f-a351-7438b169c895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e372567f-1ab9-4466-851e-e9d6f890f6e4",
            "compositeImage": {
                "id": "ee7cb7a9-45ac-4567-9ca4-961870c93bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302ee3f5-fe38-4c4f-a351-7438b169c895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccbbaf40-a4bf-4d1d-a12a-93606bb2d0d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302ee3f5-fe38-4c4f-a351-7438b169c895",
                    "LayerId": "c5bbd9bb-093b-4f96-9d75-578fc4ee1d3c"
                }
            ]
        },
        {
            "id": "ef54da1a-8b39-40a8-a8e7-7e8deb428350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e372567f-1ab9-4466-851e-e9d6f890f6e4",
            "compositeImage": {
                "id": "08f926cc-90e3-40ff-9fa4-b1c9349923b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef54da1a-8b39-40a8-a8e7-7e8deb428350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811abc0a-dab8-4202-ace5-4e891cf8a347",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef54da1a-8b39-40a8-a8e7-7e8deb428350",
                    "LayerId": "c5bbd9bb-093b-4f96-9d75-578fc4ee1d3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "c5bbd9bb-093b-4f96-9d75-578fc4ee1d3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e372567f-1ab9-4466-851e-e9d6f890f6e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}