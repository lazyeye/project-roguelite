{
    "id": "d039ae18-693e-4e7a-9851-fa1a1199e220",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spirit_stones",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 181,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6bd7327-73d9-4dfc-9e33-57e19e4b331d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d039ae18-693e-4e7a-9851-fa1a1199e220",
            "compositeImage": {
                "id": "783e7f7a-c974-4ca1-9a28-f0737a3c121c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6bd7327-73d9-4dfc-9e33-57e19e4b331d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bacb757-b5d2-404a-887c-1266d9fad2c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6bd7327-73d9-4dfc-9e33-57e19e4b331d",
                    "LayerId": "e3bed356-a551-4d4c-a0b5-095bed5790b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e3bed356-a551-4d4c-a0b5-095bed5790b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d039ae18-693e-4e7a-9851-fa1a1199e220",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 182,
    "xorig": 0,
    "yorig": 0
}