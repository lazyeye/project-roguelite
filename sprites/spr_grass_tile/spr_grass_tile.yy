{
    "id": "bd393daf-e544-421b-af3a-7e50c475c918",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grass_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa6d82c2-6e01-4649-a16b-fa5cad7813f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd393daf-e544-421b-af3a-7e50c475c918",
            "compositeImage": {
                "id": "350cbf49-2065-4b72-9145-8a2f74026c33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6d82c2-6e01-4649-a16b-fa5cad7813f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c85142-a7f3-464f-b731-6a0e3de71ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6d82c2-6e01-4649-a16b-fa5cad7813f8",
                    "LayerId": "3320288c-3a63-49e8-a0fa-4115ec8a02ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3320288c-3a63-49e8-a0fa-4115ec8a02ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd393daf-e544-421b-af3a-7e50c475c918",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}