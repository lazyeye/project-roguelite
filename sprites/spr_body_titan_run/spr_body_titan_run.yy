{
    "id": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_titan_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d9922e4-9646-46dd-841f-5d5f5ae78352",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "729db3c0-3712-4b2e-ad62-59035ef9df41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d9922e4-9646-46dd-841f-5d5f5ae78352",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7dc7215-bf9d-48a5-be0f-2b0ce9481dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d9922e4-9646-46dd-841f-5d5f5ae78352",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "1801550a-c8b3-43cd-8b59-b2b742c3c93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "ad05f21e-0e6d-4dcf-b88d-88549b19a920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1801550a-c8b3-43cd-8b59-b2b742c3c93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f45a733-d696-499f-80ba-13fb8f825591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1801550a-c8b3-43cd-8b59-b2b742c3c93f",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "7a83aaf4-32e3-4457-9ac2-9031ca0896b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "17ed02bd-7cbf-431e-ab92-905346666111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a83aaf4-32e3-4457-9ac2-9031ca0896b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3324ff84-27ff-479b-b27e-d10edada38f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a83aaf4-32e3-4457-9ac2-9031ca0896b5",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "040c498c-8d88-463b-9aac-c7c6befa0700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "b3c21a6e-f799-4cf0-be87-a12a54669f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "040c498c-8d88-463b-9aac-c7c6befa0700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1a7930-7816-41bd-a30c-b0e81bda8beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040c498c-8d88-463b-9aac-c7c6befa0700",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "1f06e386-e6cd-4f83-b575-6a20957ecee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "4a68c757-1512-4ffe-93c8-613a20cca3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f06e386-e6cd-4f83-b575-6a20957ecee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ea392d-f5df-4e93-810c-84fd91c38b1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f06e386-e6cd-4f83-b575-6a20957ecee4",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "4ad2492e-eee8-44d2-88b4-c4ca6464b76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "425a4bb9-f9bf-4b09-9ae2-31125cdc0ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ad2492e-eee8-44d2-88b4-c4ca6464b76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6efe64-07f7-4730-b0d7-455536feeadd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ad2492e-eee8-44d2-88b4-c4ca6464b76f",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "7d039be7-ccdb-439c-9371-be987f124447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "db8ca4f9-27e9-4a22-b50c-0e533219e881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d039be7-ccdb-439c-9371-be987f124447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ec7caf6-fa0d-47c6-aaa2-8d4b0c865cfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d039be7-ccdb-439c-9371-be987f124447",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "86d2675e-13be-4d79-9e0c-7cae650800ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "b7e39e15-7498-4338-ac40-89243cd736ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86d2675e-13be-4d79-9e0c-7cae650800ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0b8754-9b19-49de-8f4a-fd7fbf452910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86d2675e-13be-4d79-9e0c-7cae650800ac",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "c4a363c1-cdb6-4ed1-966d-574f1ee2eb80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "0b28173c-fbbc-4ac2-b701-30bb34ededf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a363c1-cdb6-4ed1-966d-574f1ee2eb80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8ca076-fa98-4fcb-ac00-f713301ace4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a363c1-cdb6-4ed1-966d-574f1ee2eb80",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "1c41804c-a2c1-438e-8ceb-a689f7dd9508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "37515788-2bd2-483f-bfc7-e7c478c8c1e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c41804c-a2c1-438e-8ceb-a689f7dd9508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55caed41-00e2-4e4c-8547-c99e00754c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c41804c-a2c1-438e-8ceb-a689f7dd9508",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "6c15e82c-3ba6-4c37-a375-fee5a6a72ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "d62cbf9a-9ebb-438e-9e14-e67fb0d7ee84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c15e82c-3ba6-4c37-a375-fee5a6a72ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c0a8ea3-ed94-4085-9d8b-471b1e394166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c15e82c-3ba6-4c37-a375-fee5a6a72ffb",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "91717d65-b5e0-4cb4-8e0b-eb6c9da53917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "bfb7f3b4-5f27-41ef-98be-d9c77f76f982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91717d65-b5e0-4cb4-8e0b-eb6c9da53917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3262b1-c3f7-4bb9-8baa-fffd80b626c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91717d65-b5e0-4cb4-8e0b-eb6c9da53917",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "b7af899a-35e7-4f9f-8fa8-e55694ea179f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "960abf5c-1f52-4a18-8298-3e393db062cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7af899a-35e7-4f9f-8fa8-e55694ea179f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d91d04b-24db-4207-b1eb-79ecd20b0049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7af899a-35e7-4f9f-8fa8-e55694ea179f",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "6e0a103e-6a0c-4ff0-be9d-2ef3bedcd3ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "36038b0e-76d6-4b5a-a0db-dc5b7b3bab43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0a103e-6a0c-4ff0-be9d-2ef3bedcd3ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45ab9c5c-a7d8-4e51-9ec4-b708c9b47b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0a103e-6a0c-4ff0-be9d-2ef3bedcd3ef",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "60b25b24-1ac1-48bd-a4b0-47bcaf202d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "68064337-0e24-436c-a830-7f24c5efab4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b25b24-1ac1-48bd-a4b0-47bcaf202d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21c4e6d8-e861-4d99-a262-7d8331409848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b25b24-1ac1-48bd-a4b0-47bcaf202d53",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        },
        {
            "id": "6ff08555-4147-4ef8-9847-79a31d96f5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "compositeImage": {
                "id": "2b9b90db-6923-4143-8f3d-1c494f4b3fe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff08555-4147-4ef8-9847-79a31d96f5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a71e37c7-2bec-426e-b17e-1025669fe77d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff08555-4147-4ef8-9847-79a31d96f5b0",
                    "LayerId": "176aa074-60bf-4b9d-84d3-eae98125b685"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "176aa074-60bf-4b9d-84d3-eae98125b685",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74e86178-b8e3-49a0-8e4e-0bb4b34d4782",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}