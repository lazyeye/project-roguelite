{
    "id": "a99f4eae-083e-43b9-9e62-9c1130b7f6e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5bece22-d4d5-44de-9b7d-fff140f2e960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99f4eae-083e-43b9-9e62-9c1130b7f6e1",
            "compositeImage": {
                "id": "d31405a6-b69a-4bd1-b01d-31d026bde7b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5bece22-d4d5-44de-9b7d-fff140f2e960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b182d664-cbc0-4cd1-8ad8-10f27475e55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5bece22-d4d5-44de-9b7d-fff140f2e960",
                    "LayerId": "27200891-deb1-42f9-acb7-28b484eb0767"
                }
            ]
        },
        {
            "id": "2bd7a126-edee-475a-8a87-b5ed49fc5794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99f4eae-083e-43b9-9e62-9c1130b7f6e1",
            "compositeImage": {
                "id": "d7d7abf2-2bc2-4ca2-be4d-2466fb047ba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd7a126-edee-475a-8a87-b5ed49fc5794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5db212dd-5964-43ba-a963-f716b8112f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd7a126-edee-475a-8a87-b5ed49fc5794",
                    "LayerId": "27200891-deb1-42f9-acb7-28b484eb0767"
                }
            ]
        },
        {
            "id": "6edafb60-1538-4c9a-9d9c-d040a53c076c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99f4eae-083e-43b9-9e62-9c1130b7f6e1",
            "compositeImage": {
                "id": "095e0677-1a84-430a-bc21-43ab5ec86a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6edafb60-1538-4c9a-9d9c-d040a53c076c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f158a29b-7c8b-4a7d-999d-f248cf6d19f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6edafb60-1538-4c9a-9d9c-d040a53c076c",
                    "LayerId": "27200891-deb1-42f9-acb7-28b484eb0767"
                }
            ]
        },
        {
            "id": "2a1f89e0-06dd-44d0-8096-93aecdfed4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99f4eae-083e-43b9-9e62-9c1130b7f6e1",
            "compositeImage": {
                "id": "10900936-8d2d-43bc-b210-cb941cfb4c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1f89e0-06dd-44d0-8096-93aecdfed4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41888892-7bf7-4a81-b17c-65de4197a180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1f89e0-06dd-44d0-8096-93aecdfed4c4",
                    "LayerId": "27200891-deb1-42f9-acb7-28b484eb0767"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "27200891-deb1-42f9-acb7-28b484eb0767",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a99f4eae-083e-43b9-9e62-9c1130b7f6e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 8
}