{
    "id": "0585f655-6278-4ec6-bf11-bfc8180de460",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_body_steel_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 10,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b726e66-cbb0-4f64-95f9-7d2f6a6d8879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0585f655-6278-4ec6-bf11-bfc8180de460",
            "compositeImage": {
                "id": "f99271eb-d2ee-42e9-bc0c-2ce923634518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b726e66-cbb0-4f64-95f9-7d2f6a6d8879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "463c0932-da83-4fda-9587-fda9f498c9b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b726e66-cbb0-4f64-95f9-7d2f6a6d8879",
                    "LayerId": "e1fe8eab-f17a-4d02-8473-cd1cc4b3e5a0"
                }
            ]
        },
        {
            "id": "62378729-d589-4e81-8c57-656ac2b15a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0585f655-6278-4ec6-bf11-bfc8180de460",
            "compositeImage": {
                "id": "8d70a905-6342-4726-bd38-bb0ea823780a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62378729-d589-4e81-8c57-656ac2b15a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc21ce73-1526-4e79-b0a0-7c9e5f594f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62378729-d589-4e81-8c57-656ac2b15a6a",
                    "LayerId": "e1fe8eab-f17a-4d02-8473-cd1cc4b3e5a0"
                }
            ]
        },
        {
            "id": "6e7c6edd-2fa2-4747-ab12-fc1d6a524041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0585f655-6278-4ec6-bf11-bfc8180de460",
            "compositeImage": {
                "id": "1861cde0-3842-4c3d-94a5-aefb56e17697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7c6edd-2fa2-4747-ab12-fc1d6a524041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "476addab-a035-4b87-beb2-31b9c3da86fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7c6edd-2fa2-4747-ab12-fc1d6a524041",
                    "LayerId": "e1fe8eab-f17a-4d02-8473-cd1cc4b3e5a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "e1fe8eab-f17a-4d02-8473-cd1cc4b3e5a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0585f655-6278-4ec6-bf11-bfc8180de460",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}