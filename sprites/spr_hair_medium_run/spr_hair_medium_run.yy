{
    "id": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_medium_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15da51fc-e47c-4590-8494-d70269684978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "8536a131-7f8c-4e9f-9f94-85d7e12c0abf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15da51fc-e47c-4590-8494-d70269684978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c97b991-31ca-4da0-8412-6f48d7157e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15da51fc-e47c-4590-8494-d70269684978",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "57497fdc-b3c1-4df0-8a3b-a4d4d06f817b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "f469b145-3835-4574-ae06-efb8a29b8766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57497fdc-b3c1-4df0-8a3b-a4d4d06f817b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095a4cdd-2fa9-44fd-aa23-b4d08caf90c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57497fdc-b3c1-4df0-8a3b-a4d4d06f817b",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "a6214937-b2a4-44bb-aa82-fe8cb92d8750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "5ef7cf51-b41c-489d-9917-10ecdaadd903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6214937-b2a4-44bb-aa82-fe8cb92d8750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc314ea-321a-439f-817d-a9ff32394cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6214937-b2a4-44bb-aa82-fe8cb92d8750",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "c1e0b4e1-f1e9-4686-906c-62803f2db061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "c025cc1a-e4d6-45fb-b55d-87ad1dbc2962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e0b4e1-f1e9-4686-906c-62803f2db061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a07d34-6ab7-4644-9b20-2dd5d5e5d9c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e0b4e1-f1e9-4686-906c-62803f2db061",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "82b28b26-bae2-4893-a1df-432f9e307ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "9f643eaa-f30c-4477-980e-81bdd6aac483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b28b26-bae2-4893-a1df-432f9e307ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a7888a-f790-43d7-9018-539f2bfa67ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b28b26-bae2-4893-a1df-432f9e307ef1",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "80edd673-cc6d-4cce-8b4e-ff962d315f08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "d334ebbc-2d85-415e-a57d-1db5435a9961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80edd673-cc6d-4cce-8b4e-ff962d315f08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f310f9-3feb-4b1d-be70-6367717fe9bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80edd673-cc6d-4cce-8b4e-ff962d315f08",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "e0b15232-f3d8-41f1-8568-fb4ca2b967f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "d100415b-81ed-4164-b123-e81b81926e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b15232-f3d8-41f1-8568-fb4ca2b967f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e4560df-a9b9-44f0-a293-aaf7a1fe8924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b15232-f3d8-41f1-8568-fb4ca2b967f7",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "79d26ae6-45c1-419d-b3c2-a52464dd287a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "58cdfe96-5ba4-4bba-9f14-e802066ec8d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79d26ae6-45c1-419d-b3c2-a52464dd287a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04e3114e-fe4a-4a73-9d40-2060fee42fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79d26ae6-45c1-419d-b3c2-a52464dd287a",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "8f7dd66b-66d1-4e88-a141-a3e88900fceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "a4a69b96-c36b-4f17-92b1-3961a7a9da6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f7dd66b-66d1-4e88-a141-a3e88900fceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f777ed60-cf70-4182-837d-6c1e5a78c999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f7dd66b-66d1-4e88-a141-a3e88900fceb",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "6e7ec050-bf80-4ab9-ac9f-11d1da0996f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "adf1bf66-9b85-4988-b61a-92d77a8f863d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e7ec050-bf80-4ab9-ac9f-11d1da0996f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0a3732-c73c-4ef6-80a5-add86d5b3102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e7ec050-bf80-4ab9-ac9f-11d1da0996f0",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "5083b802-ec1a-466d-a05e-3dc04bbaa40c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "e1c11c5a-bb84-4656-99a2-771c830aacd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5083b802-ec1a-466d-a05e-3dc04bbaa40c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6f17876-a9a1-4f9e-93fc-dbcf451cabae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5083b802-ec1a-466d-a05e-3dc04bbaa40c",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "90402289-4cc2-4ee8-bf67-0a7da4d5240d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "8a487d72-ac7f-4845-86b9-c2632ed4e63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90402289-4cc2-4ee8-bf67-0a7da4d5240d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d4766ed-997e-4eeb-b6f3-918ab8f42825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90402289-4cc2-4ee8-bf67-0a7da4d5240d",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "2e50c5ca-6625-4b8f-9a24-55e5a1f31e0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "c70f9a5f-30e9-40c8-b5d8-0849b25fea22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e50c5ca-6625-4b8f-9a24-55e5a1f31e0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5481eb33-4018-40b0-9c49-c693d7405a83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e50c5ca-6625-4b8f-9a24-55e5a1f31e0d",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "9dd3def0-29ac-459d-bf91-28c257b2823a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "5912f419-b404-415d-a487-894ef4ce3950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd3def0-29ac-459d-bf91-28c257b2823a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01f9d19-577b-47b0-a342-d030d7108dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd3def0-29ac-459d-bf91-28c257b2823a",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "ff3c93a8-f70a-4732-aac1-5ed3ef1f52bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "172b13c0-8296-460f-a42c-c843aabb79a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3c93a8-f70a-4732-aac1-5ed3ef1f52bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e47c665-142f-4edf-9472-7699f8483d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3c93a8-f70a-4732-aac1-5ed3ef1f52bd",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        },
        {
            "id": "86c02bd5-707e-4a2f-97fd-4addaf33a529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "compositeImage": {
                "id": "def4f8c7-401c-411a-a0ca-1a1cc3a1d605",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86c02bd5-707e-4a2f-97fd-4addaf33a529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36d6d54-6beb-4b4c-948f-bba8312c6061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86c02bd5-707e-4a2f-97fd-4addaf33a529",
                    "LayerId": "3903e02b-0917-41c3-bb89-e85114a7cd7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "3903e02b-0917-41c3-bb89-e85114a7cd7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da6b4454-bf5a-41b0-811f-1f78719e4d68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}