{
    "id": "c31c94d7-182c-47b9-805b-14b3e6b93267",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_short_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 4,
    "bbox_right": 9,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfcb2d24-bc96-416e-9ab3-0c0fe1e4a868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31c94d7-182c-47b9-805b-14b3e6b93267",
            "compositeImage": {
                "id": "b09eae6e-e2c4-49d5-9e73-2e0ee852a33b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfcb2d24-bc96-416e-9ab3-0c0fe1e4a868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c227d35-9b32-406d-9dfe-d0908c6a5837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfcb2d24-bc96-416e-9ab3-0c0fe1e4a868",
                    "LayerId": "b3ae447f-fb90-48f6-8a76-45097237f7ce"
                }
            ]
        },
        {
            "id": "a7b39bbb-f611-4168-86c1-1ebddd23f3c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31c94d7-182c-47b9-805b-14b3e6b93267",
            "compositeImage": {
                "id": "a4f6de6a-c2f0-4975-9887-4193f37cc936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b39bbb-f611-4168-86c1-1ebddd23f3c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35875784-300e-499c-8943-0eca7c2a119b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b39bbb-f611-4168-86c1-1ebddd23f3c0",
                    "LayerId": "b3ae447f-fb90-48f6-8a76-45097237f7ce"
                }
            ]
        },
        {
            "id": "91922adf-bf62-4e35-810a-bfe58d960c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31c94d7-182c-47b9-805b-14b3e6b93267",
            "compositeImage": {
                "id": "1e244217-4994-4d39-be52-ca14d84dcd50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91922adf-bf62-4e35-810a-bfe58d960c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd577535-2911-40e4-8f35-212c02068e14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91922adf-bf62-4e35-810a-bfe58d960c12",
                    "LayerId": "b3ae447f-fb90-48f6-8a76-45097237f7ce"
                }
            ]
        },
        {
            "id": "af4eeb4b-8cc6-4862-b399-d1f85ecba322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31c94d7-182c-47b9-805b-14b3e6b93267",
            "compositeImage": {
                "id": "df5d0d51-e09f-47c5-a1c5-c0aaf262e2b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af4eeb4b-8cc6-4862-b399-d1f85ecba322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ec5178-d542-4e23-a303-f58bf96eac63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af4eeb4b-8cc6-4862-b399-d1f85ecba322",
                    "LayerId": "b3ae447f-fb90-48f6-8a76-45097237f7ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "b3ae447f-fb90-48f6-8a76-45097237f7ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c31c94d7-182c-47b9-805b-14b3e6b93267",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}