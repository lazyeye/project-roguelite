{
    "id": "b31a6c75-ef81-41d6-86fa-89c0abc30331",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_spiky_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 5,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f29da04d-57ac-4fad-bce5-5639cd4e6bd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31a6c75-ef81-41d6-86fa-89c0abc30331",
            "compositeImage": {
                "id": "fcf02485-9aa0-498e-8aee-7861550038af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f29da04d-57ac-4fad-bce5-5639cd4e6bd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02825090-e937-44f2-8c22-736bcf4d9d51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f29da04d-57ac-4fad-bce5-5639cd4e6bd7",
                    "LayerId": "2d30937e-2455-4759-8a38-2549fa902b80"
                }
            ]
        },
        {
            "id": "4fd7f0e4-578d-4b00-b1fc-dde2e078f49a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31a6c75-ef81-41d6-86fa-89c0abc30331",
            "compositeImage": {
                "id": "e9d2b78f-c02d-45e0-8c10-3f62ca8d5d2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd7f0e4-578d-4b00-b1fc-dde2e078f49a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e3e39dd-01e8-4f11-bef0-117010a781fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd7f0e4-578d-4b00-b1fc-dde2e078f49a",
                    "LayerId": "2d30937e-2455-4759-8a38-2549fa902b80"
                }
            ]
        },
        {
            "id": "5eb634e6-e019-4978-92f1-25583c5b2b87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b31a6c75-ef81-41d6-86fa-89c0abc30331",
            "compositeImage": {
                "id": "214809b7-0621-474e-b74a-7dfc3189be32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eb634e6-e019-4978-92f1-25583c5b2b87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6550dd2d-b1e9-4e6f-8f45-c07458a5215c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eb634e6-e019-4978-92f1-25583c5b2b87",
                    "LayerId": "2d30937e-2455-4759-8a38-2549fa902b80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "2d30937e-2455-4759-8a38-2549fa902b80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b31a6c75-ef81-41d6-86fa-89c0abc30331",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}