{
    "id": "8a970133-14ba-407a-b8fc-c996a8b2b263",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_long_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2817d97-30b7-4461-bfa1-257486474c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "24b3306a-965c-4732-9cbf-b2a94ca44c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2817d97-30b7-4461-bfa1-257486474c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b035e78e-7c10-4d58-8ac3-553bef7840f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2817d97-30b7-4461-bfa1-257486474c2c",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "a03bc4ec-436c-46f2-8c6d-fd376502359f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "c8d73c06-2010-4c09-ba08-5625fc065fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a03bc4ec-436c-46f2-8c6d-fd376502359f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee31ca6-5f50-448b-97f5-3f30b816951a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a03bc4ec-436c-46f2-8c6d-fd376502359f",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "54098d66-6b85-4fc6-86ab-257204425b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "00ad9268-ae40-411c-8958-9dff7ea877f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54098d66-6b85-4fc6-86ab-257204425b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0b8b09-16f1-4b61-b238-8d3564f61058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54098d66-6b85-4fc6-86ab-257204425b9b",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "d36d5781-85b3-474d-ad9b-ee6b48b1343b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "e2f46fea-4f2d-42b5-a963-728c9c42c06f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d36d5781-85b3-474d-ad9b-ee6b48b1343b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bea565-abe5-46a0-ac27-e68365406fcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d36d5781-85b3-474d-ad9b-ee6b48b1343b",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "37ff4ce7-e35e-4a0a-aa2e-4c5ea2a99a00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "733995f7-6bf5-4617-8692-1df957bc3ca1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37ff4ce7-e35e-4a0a-aa2e-4c5ea2a99a00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0173ac4e-d7a4-4f4b-980b-1060be255a0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37ff4ce7-e35e-4a0a-aa2e-4c5ea2a99a00",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "24dd49bf-3af7-4b10-8ffc-4bfa8d964bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "bcbf1aef-8a28-4408-958c-d22807ea0f1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24dd49bf-3af7-4b10-8ffc-4bfa8d964bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e607704-ee18-4e8f-9018-84e905d343b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24dd49bf-3af7-4b10-8ffc-4bfa8d964bba",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "36bf7b06-f994-47cf-9d96-d4ba1685813c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "8d7ee18c-f454-4e7a-be11-ad80d71ae445",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36bf7b06-f994-47cf-9d96-d4ba1685813c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "301f7747-16f1-41c3-b512-46d63eab0679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36bf7b06-f994-47cf-9d96-d4ba1685813c",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "04a7596d-80cc-4f63-9936-d08e8b6c5897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "f9f9ffb2-f2b5-4124-9082-6bffa8fcb176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04a7596d-80cc-4f63-9936-d08e8b6c5897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36489692-b0c0-4056-9d76-55e9ec41fa60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04a7596d-80cc-4f63-9936-d08e8b6c5897",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "7efc803c-44e0-4035-bab5-8b0984fbe8da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "1abf0fae-11fb-482f-83c8-d05d77555d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7efc803c-44e0-4035-bab5-8b0984fbe8da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b616054-9753-4743-939b-e918295ef256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7efc803c-44e0-4035-bab5-8b0984fbe8da",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "688dac6d-86ae-4aff-a1e2-04c4c97f6da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "d9d59608-9d14-4cc0-b51f-39fa579e4dad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688dac6d-86ae-4aff-a1e2-04c4c97f6da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92fcb4bf-c358-426f-ae08-bb31414ec60e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688dac6d-86ae-4aff-a1e2-04c4c97f6da2",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "4e279dab-7786-4e03-bae3-e45a13c2be11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "9254b138-a843-4289-aaf4-c6016a9ef553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e279dab-7786-4e03-bae3-e45a13c2be11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1892e593-1a7e-4fea-80a6-66f3587bff3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e279dab-7786-4e03-bae3-e45a13c2be11",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "68a4c708-94cc-4bf7-bb5f-87bdefdea80e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "9915279e-3d8a-422e-8edc-6944989e3c15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68a4c708-94cc-4bf7-bb5f-87bdefdea80e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ccf66d-0f9e-446d-82a3-5a3c2c2eeca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68a4c708-94cc-4bf7-bb5f-87bdefdea80e",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "ac5c4679-2c44-4264-b170-967e7ac785fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "c9b63619-10ae-4324-a74e-e5a76a4239a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac5c4679-2c44-4264-b170-967e7ac785fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04518563-ce96-4590-87f0-415bd601fbc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac5c4679-2c44-4264-b170-967e7ac785fd",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "4319080d-b93f-4367-9b80-0b98446341b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "bf77a64c-9d12-4a9f-aafa-a3bae9b41bc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4319080d-b93f-4367-9b80-0b98446341b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90386ee3-4069-47b6-a4ef-dcf58ebfd492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4319080d-b93f-4367-9b80-0b98446341b4",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "56431962-b05a-47a8-88af-e1cd63a147da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "d0c99e41-ad3f-4437-86ad-a83b20633aa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56431962-b05a-47a8-88af-e1cd63a147da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd6490b-0164-4577-8575-e2ee1ece3297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56431962-b05a-47a8-88af-e1cd63a147da",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        },
        {
            "id": "9ebfaec3-bbff-41a0-89af-f4eca2062c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "compositeImage": {
                "id": "29401996-8a02-441d-a66b-f58574fe965e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ebfaec3-bbff-41a0-89af-f4eca2062c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e203e13-efe6-47ce-bb29-439a75d1ed9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ebfaec3-bbff-41a0-89af-f4eca2062c56",
                    "LayerId": "55a06828-1901-43cb-b1a6-84a4e6f67084"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "55a06828-1901-43cb-b1a6-84a4e6f67084",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a970133-14ba-407a-b8fc-c996a8b2b263",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}