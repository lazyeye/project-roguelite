{
    "id": "7047d725-c381-4154-88d8-b4aa8f828c10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 6,
    "bbox_right": 9,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e88f5972-b33b-4a3a-bd51-3f40b4c7fe1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047d725-c381-4154-88d8-b4aa8f828c10",
            "compositeImage": {
                "id": "3446b092-daf9-4f93-9d40-ed124498d8bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e88f5972-b33b-4a3a-bd51-3f40b4c7fe1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae8d1e6-86da-40c7-b2d0-63314dcc9a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e88f5972-b33b-4a3a-bd51-3f40b4c7fe1a",
                    "LayerId": "57338fdc-c384-4c44-8714-0cc43a02c8ba"
                }
            ]
        },
        {
            "id": "262dd460-c0e8-4cd9-b00e-2f4f9f29cdbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047d725-c381-4154-88d8-b4aa8f828c10",
            "compositeImage": {
                "id": "13911f9d-9420-4c77-9132-5bbc9215c2a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "262dd460-c0e8-4cd9-b00e-2f4f9f29cdbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08eb87ec-bced-4b7a-82a0-284feb912de0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "262dd460-c0e8-4cd9-b00e-2f4f9f29cdbd",
                    "LayerId": "57338fdc-c384-4c44-8714-0cc43a02c8ba"
                }
            ]
        },
        {
            "id": "de491296-41d6-45ae-b5dd-2ba52227edd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7047d725-c381-4154-88d8-b4aa8f828c10",
            "compositeImage": {
                "id": "4757318f-35aa-4740-a4c3-41152ad8f02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de491296-41d6-45ae-b5dd-2ba52227edd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f1f9b0-aa86-447b-bab7-9c1bbaaff4b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de491296-41d6-45ae-b5dd-2ba52227edd3",
                    "LayerId": "57338fdc-c384-4c44-8714-0cc43a02c8ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "57338fdc-c384-4c44-8714-0cc43a02c8ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7047d725-c381-4154-88d8-b4aa8f828c10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}