{
    "id": "6b925851-2a6e-4782-8577-ade8ebc86c37",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_main",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b03",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2d7e7aae-873f-40b0-a416-fceb4ab90e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "55fe1e84-1e60-4d94-b146-b734d5e8c343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 15,
                "y": 42
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "cc8ebedf-2ac8-4aaa-83f3-0405ea46da10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 32
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3637313f-caa4-4b66-aaad-cce43d3ab617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e224e1b5-8114-4508-ae2c-8ac79a36c72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 22
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c00e6044-615b-4c4f-8d4e-a315c301dfbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "80c851f4-93a6-42b9-a808-4eb8931795c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "65da6d30-47a7-4de3-9b30-511082dd56b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 9,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1428af62-4e53-4170-9ec0-02b28137c4b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 113,
                "y": 32
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "724e5845-97db-40e8-815e-9053772195d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ddf36c8b-0709-4a72-8086-4dcb7fda9060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 110,
                "y": 22
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "66088196-f931-4cca-b5ec-022c4a30071f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 27,
                "y": 32
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "56934bca-caf4-44bb-9923-c872923f47d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ee33af56-fb91-4f83-9b8d-83a4ea89c1b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 115,
                "y": 22
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6937d1fa-e898-4e4c-afe2-b5a459abf537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 27,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ed554193-8652-41da-afeb-6dc5d23d142c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "557a045d-fd32-410d-b837-d4220c207562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "31d0e13e-7204-4025-aaed-83acb2943bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 105,
                "y": 32
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bdf65156-d528-4978-a790-11095264454b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 56,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8e1ba0da-0fe9-4542-a379-8a2e0cb204b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 44,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e0c98317-777c-43f2-9923-2ff2769e19bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 32,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "de02e89b-96e0-4383-b74a-8fca4d4f314c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3fef2489-74b8-47ea-8e5f-19ec699c114c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c91893e0-e33a-4f28-8df8-fd93c1b6935e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 22
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e8b98476-e00b-4716-b891-31001fce1ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c088247e-a44d-4faf-8bea-03535f01a3d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "620b1a81-014a-487f-9e87-4f3c027b0255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 12,
                "y": 42
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b6524253-67a2-43f8-90d1-920a9e51c1d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 18,
                "y": 42
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8dafc483-f7e2-4185-a29d-4efa1c5005df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 77,
                "y": 32
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "03e8c6ea-e7ea-4f17-a44d-dbba73612705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 82,
                "y": 32
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3e63a0c9-e351-4e1a-a595-7b75657ad7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 32
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "276b18e5-e5a9-4755-9980-661f6d09871c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "837bb754-ec56-45c1-a429-bd208473ee24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2818ceae-0500-432e-8ef7-e2260522a02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "264bc23c-3931-41f2-9c0a-3647200b597a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "86e69418-0f90-48dc-9cf5-04daf450f96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 57,
                "y": 32
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7e0a6cf6-be68-4658-a84a-cbaaf501a3c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 22
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c87def4c-ebd5-47c4-a7a4-6cc968168a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 32
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dc11c3c6-0134-4995-be4c-a3da721a0735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 42,
                "y": 32
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6f04927c-60ad-48bc-8295-6e347a6b32b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e66b77b8-a8ef-4467-a9b6-22faa0d07a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6e3ac61d-945e-4b83-a82a-ddd655a6304f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 87,
                "y": 32
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ef0bf67f-27ba-4780-8f85-b1e74f7526d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 104,
                "y": 12
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "654ac0d9-dc65-4400-ad55-ec157c89f4c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 12
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7ad3ecb5-0c76-4b0b-9376-308de7fcd2bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 32
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3aa26c88-75e6-41ca-bf65-9d1eebb9bd83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cb7fc319-a73a-4098-a0d0-2424f25b067f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d95341c9-ad88-4de7-af00-950d78f732e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 22
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "970928d1-611d-491f-b715-d192f8f7f1c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "dd7baa00-430f-4c18-ac81-fc05e122274c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ab5e723e-7f98-407d-a5df-f85faecc7cd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "37cb400f-4321-4914-a9db-92a24b573786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2e070dce-5dce-4fa2-909d-80fdaf62b320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 72,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b1232629-0cfe-47fd-a9c7-43f692a8373d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 8,
                "y": 12
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "830f75b0-1349-4009-b6f8-1812bf84c5a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1a507241-1066-43f8-a71c-118191b71912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "67a8b11a-df29-48da-bec7-263adda503c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "be4afe43-9cc4-40e7-9593-37b641f0ffba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e2165bf8-d5f9-4166-81aa-77105d909dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 92,
                "y": 32
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a60f3685-3119-4a6c-9ea1-6ea2f347d6ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 101,
                "y": 32
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0bb441cf-7c8c-496a-802d-e43d1641c3fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2ef1c5f0-c2b6-4a5e-ba87-4c5e9a6701e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 97,
                "y": 32
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fbf79a99-be08-4759-9743-dafb468cdb78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 37,
                "y": 32
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f30e7e4a-04d0-4462-906a-16228eba0128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 12
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "dd5ee8d2-92fd-4343-bcb5-dc658f992f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 121,
                "y": 32
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "81842661-db87-45eb-b430-3fe1bc4297f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 68,
                "y": 12
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5e996e3f-95e2-4692-ab1c-176ddd8424e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 12
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bfdf7696-edff-45b1-9276-44fd020c5ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 32,
                "y": 32
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8b65f887-a0c8-416f-adfd-a82877329024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 110,
                "y": 12
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "777f566e-6f88-48b1-aff2-0db1a75c77a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 20,
                "y": 12
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9548fc91-aa21-4281-8b78-cff40f2a8f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7f06f2a1-ce77-4938-b940-356ac2f34265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 98,
                "y": 12
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f8b19d1e-8e75-4278-86b9-9a8ea33fa7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 92,
                "y": 12
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a65e7095-8645-4b3d-bbe6-2784de6b24e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "bca1dbfb-6f8d-4982-9fba-d18759803cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 8,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 117,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e5f92f22-6100-4e72-977a-c350c4519663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 12
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5dfaa70d-3039-4381-b63e-67f9aa62c38c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 6,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6ba6109a-4508-4fd4-b1ed-5f31841776c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "09d67267-fe3e-42cc-b54a-9aff54c62567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 12
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "806a40e5-c199-4ae9-8ec9-0c3db3c040e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 116,
                "y": 12
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "295c2ab5-0798-4d6b-8f13-f83f1aaf1479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 74,
                "y": 12
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ddd1447f-b3a0-4b3a-8c1b-626222378915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 62,
                "y": 12
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4faedd86-8d08-4893-86e6-7dde999b0e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 22,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e87a3377-3aa8-4c8f-ad8e-7c708fc3c952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 56,
                "y": 12
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "41d5d62d-d0fa-442c-ad17-e98ed23cd999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 120,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c264ef98-89d2-44ad-b0c5-49e18fcb8f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 50,
                "y": 12
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "addb7b4f-b67a-4cd9-98ab-684397633371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 44,
                "y": 12
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8f37b8be-f701-417b-b19c-3f16024adef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 8,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "11a57eb7-504d-4b3e-9645-11b5d06a5732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d6eec8ab-9c75-42c3-9c10-216b07c9c43e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 38,
                "y": 12
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f66aa327-16bf-44b7-8e8d-d77c588c0a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 32,
                "y": 12
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7f06fd89-b4ed-418a-ae6e-765b4c19bba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 17,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "da4ce7a8-83d5-4b70-91c3-0565916c7792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 8,
                "offset": 0,
                "shift": 2,
                "w": 1,
                "x": 21,
                "y": 42
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dbeb48b2-7465-4913-9a30-3a056bc2af11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 8,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 62,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "50ccbc1d-156a-4125-9723-a28cc7384c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 8,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 14,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}