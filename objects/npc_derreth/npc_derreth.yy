{
    "id": "c4fdb5d2-4b1b-44cf-ad10-7f1716ada889",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "npc_derreth",
    "eventList": [
        {
            "id": "1370e651-bb96-43f9-8457-f43cfec906f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c4fdb5d2-4b1b-44cf-ad10-7f1716ada889"
        },
        {
            "id": "99bd590b-d841-44e0-8521-02ebc404b958",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c4fdb5d2-4b1b-44cf-ad10-7f1716ada889"
        },
        {
            "id": "86e78ec7-362f-4619-8a87-fde535869dba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4fdb5d2-4b1b-44cf-ad10-7f1716ada889"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fa306f6b-63d5-4fa7-b6f2-9f7d65399294",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc844da1-7b77-4736-960b-0c9dd662e834",
    "visible": true
}