event_inherited();

// Data
dia_add("default",
"Hello there! This is some basic dialogue.",
"Anyway, just wanted to test this out.",
"Try talking to me again to see if this really worked!");

dia_add("introduced",
"Well, well, well, it looks like it worked!");
if (data_ref[? object_ref] = "null") // If this game was loaded (and we should not init a value)
{
	data_ref[? object_ref] = "default";
}

// Sprite
hair = irandom(ds_list_size(global.cc_hair_list) - 1);
hair_color = pal_swap_get_color_count(pal_hair)
body = irandom(ds_list_size(global.cc_body_list) - 1);
var _map_index = global.cc_body_list[| body];
var _pal = asset_get_index(_map_index[? "Palette"]);
body_color = pal_swap_get_color_count(_pal);
sprite_index = cc_create_sprite("Idle", 0, 3);
image_index = 0;