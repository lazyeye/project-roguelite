{
    "id": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_depth_instance",
    "eventList": [
        {
            "id": "b2037aa3-865f-4ebe-b635-88072c9c5bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664"
        },
        {
            "id": "c583e20f-3dba-47b3-bcfd-9e0147b1728d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664"
        },
        {
            "id": "706fed66-4ccd-4a00-b363-eda01f3906f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}