// Depth updates
if (dynamic)
{
	if (x != xprevious or y != yprevious)
	{
		draw_array[0] = id;
		draw_array[1] = object_index;
		if (not is_undefined(ds_priority_find_priority(global.draw_queues[global.depth_x, global.depth_y], draw_array)))
		{
			ds_priority_change_priority(global.draw_queues[global.depth_x, global.depth_y], draw_array, bbox_bottom);
		}
		else
		{
			ds_priority_add(global.draw_queues[global.depth_x, global.depth_y], draw_array, bbox_bottom);
			ds_priority_delete_value(global.draw_queues[draw_x_previous, draw_y_previous], draw_array);
		}
	}
	draw_x_previous = global.depth_x;
	draw_y_previous = global.depth_y;
}