if (live_call()) return live_result;

if (mouse_check_button(mb_left))
{
	if (position_meeting(mouse_x, mouse_y, id))
	{
		index = 1;
	}
}
else
{
	index = 0;
}

if (mouse_check_button_pressed(mb_left))
{
	if (position_meeting(mouse_x, mouse_y, id))
	{
		switch (category)
		{
			case "hair":
				obj_cc.hair += image_xscale;
				break;
			case "hair color":
				obj_cc.hair_color += image_xscale;
				break;
			case "body":
				obj_cc.body += image_xscale;
				break;
			case "body color":
				obj_cc.body_color += image_xscale;
				break;
		}
	}
}