{
    "id": "df346c13-c735-4fd2-930f-06c44ef66f9b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cc_arrow",
    "eventList": [
        {
            "id": "f5df4afa-92e3-4675-90ae-b174d624265b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df346c13-c735-4fd2-930f-06c44ef66f9b"
        },
        {
            "id": "cbd71548-caaf-429d-ae70-f8d7298ad861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df346c13-c735-4fd2-930f-06c44ef66f9b"
        },
        {
            "id": "c63573ff-40ce-478d-bb18-f5927c94d4d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df346c13-c735-4fd2-930f-06c44ef66f9b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "abe24259-30e3-458c-82e5-b18c39a190d2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "\"null\"",
            "varName": "category",
            "varType": 2
        }
    ],
    "solid": false,
    "spriteId": "bbc9bdbc-1778-42dd-838b-005243e766da",
    "visible": true
}