{
    "id": "57d0d940-baca-4ed1-8bbd-666aec3efba1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "npc_general",
    "eventList": [
        {
            "id": "1976c812-f883-4b79-b73d-e93ac2283205",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57d0d940-baca-4ed1-8bbd-666aec3efba1"
        },
        {
            "id": "7c926810-b848-45a3-8770-d2ab956faaf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57d0d940-baca-4ed1-8bbd-666aec3efba1"
        },
        {
            "id": "94a72a42-0d0c-49ec-a66a-05d99804a5ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "57d0d940-baca-4ed1-8bbd-666aec3efba1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fa306f6b-63d5-4fa7-b6f2-9f7d65399294",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc844da1-7b77-4736-960b-0c9dd662e834",
    "visible": true
}