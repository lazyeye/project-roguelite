event_inherited();

// Data
dia_add("general_one", "Hello! It's a lovely day, isn't it?");
dia_add("general_two", "Hi!");
dia_add("general_three", "I love this place. The nature is so comforting.");
if (data_ref[? object_ref] = "null") // If the object is not being loaded and should have init data
{
	data_ref[? object_ref] = "general_one";
}

// Sprite
hair = irandom(ds_list_size(global.cc_hair_list) - 1);
hair_color = pal_swap_get_color_count(pal_hair)
body = irandom(ds_list_size(global.cc_body_list) - 1);
var _map_index = global.cc_body_list[| body];
var _pal = asset_get_index(_map_index[? "Palette"]);
body_color = pal_swap_get_color_count(_pal);
sprite_index = cc_create_sprite("Idle", 0, 3);
image_index = 0;