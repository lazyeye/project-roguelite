if (live_call()) return live_result;

draw_self();

// Wrap values
hair = wrap(hair, 0, ds_list_size(global.cc_hair_list) - 1);
body = wrap(body, 0, ds_list_size(global.cc_body_list) - 1);

// Draw player preview
var _x = x + 30;
var _y = y + 22;
var _body_map = global.cc_body_list[| body];
var _body_index = _body_map[? "Idle"];
var _body_pal_index = _body_map[? "Palette"];
var _body_pal = asset_get_index(_body_pal_index);
var _body = asset_get_index(_body_index);
body_color = wrap(body_color, 0, pal_swap_get_pal_count(_body_pal) - 1);
pal_swap_set(_body_pal, body_color, false)
{
	draw_sprite(_body, 0, _x, _y);
}
pal_swap_reset();

draw_sprite(spr_head_idle, 0, _x, _y);

var _hair_map = global.cc_hair_list[| hair];
var _hair_index = _hair_map[? "Idle"];
var _hair_pal_index = _hair_map[? "Palette"];
var _hair_pal = asset_get_index(_hair_pal_index);
var _hair = asset_get_index(_hair_index);
hair_color = wrap(hair_color, 0, pal_swap_get_pal_count(_hair_pal) - 1);
pal_swap_set(_hair_pal, hair_color, false)
{
	draw_sprite(_hair, 0, _x, _y);
}
pal_swap_reset();

// Draw name
var _x = x + (sprite_get_width(sprite_index) / 2);
var _y = 16;
draw_set_color(pal_white);
draw_set_font(fnt_main);
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_text(_x, _y, name);