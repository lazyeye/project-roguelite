if (live_call()) return live_result;

if (text_input)
{
	name = keyboard_string;
}

if (pressed(vk_enter))
{
	if (text_input)
	{
		text_input = false;
	}
	else
	{
		with (instance_create_depth(0, 0, -100, obj_player))
		{
			name = other.name;
			hair = other.hair;
			hair_color = other.hair_color;
			body = other.body;
			body_color = other.body_color;
			
			generate_player_sprites();

			sprite_index = spr_player_idle_right;
		}
		room_goto_next();
	}
}

if (mouse_left)
{
	var _x = x + (sprite_get_width(sprite_index) / 2);
	var _y = 16;
	if (text_input)
	{
		if (not point_in_rectangle(mouse_x, mouse_y, _x - 35, _y - 5, _x + 35, _y + 5))
		{
			text_input = false;
		}
	}
	else if (point_in_rectangle(mouse_x, mouse_y, _x - 35, _y - 5, _x + 35, _y + 5))
	{
		text_input = true;
		name = "";
		keyboard_string = "";
	}
}