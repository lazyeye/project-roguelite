{
    "id": "a96b60fa-a370-4925-8537-a7026e8d9923",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cc",
    "eventList": [
        {
            "id": "01bc86f3-d1aa-45b9-a00a-6e2bb71dc310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a96b60fa-a370-4925-8537-a7026e8d9923"
        },
        {
            "id": "d2cd435b-e167-4702-ae74-2a19c9f59d0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a96b60fa-a370-4925-8537-a7026e8d9923"
        },
        {
            "id": "5f6a50e2-7edc-41b0-93a0-6cb90bb2784b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a96b60fa-a370-4925-8537-a7026e8d9923"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b3621cce-5c70-45ae-9163-c95e727ee062",
    "visible": true
}