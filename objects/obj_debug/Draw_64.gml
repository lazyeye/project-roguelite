if (debug_menu)
{
	draw_set_color(c_black);
	draw_set_alpha(0.5);
	draw_rectangle(0, global.view_h * (2 / 3), global.view_w, global.view_h, false);
	draw_set_alpha(1);
	draw_set_color(c_white);
	draw_set_font(fnt_main);
	draw_set_valign(fa_middle);
	draw_set_halign(fa_left);
	draw_text(10, global.view_h - 40, debug_return);
	draw_text(10, global.view_h - 10, keyboard_string);

}