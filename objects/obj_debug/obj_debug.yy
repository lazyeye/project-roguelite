{
    "id": "be81972c-91a5-4c73-913b-ba56646b1cb4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debug",
    "eventList": [
        {
            "id": "61f7f1b6-01fd-4074-9211-f8a8fa8b9ff4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be81972c-91a5-4c73-913b-ba56646b1cb4"
        },
        {
            "id": "339f62c1-3aa2-407b-a92d-be29ecdca070",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be81972c-91a5-4c73-913b-ba56646b1cb4"
        },
        {
            "id": "fe5d1027-e8ae-4e92-b45e-7933539a1dab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "be81972c-91a5-4c73-913b-ba56646b1cb4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}