// Debug Menu
if (global.debug)
{
	if (pressed(192))
	{
		debug_menu = !debug_menu;
		if (instance_exists(obj_player))
		{
			if (debug_menu) obj_player.locked = true;
			if (not debug_menu) obj_player.locked = false;
		}
		keyboard_string = "";
		debug_return = "Type your code below.";
	} 

	if (debug_menu)
	{
		if (pressed(vk_enter))
		{
			// String storage
			var _key_str = keyboard_string;
			
			// Parse commands
			var _str = "spawn ";
			if (string_pos(_str, _key_str))
			{
				_key_str = string_replace(_key_str, _str, "");
				var _inst = asset_get_index(_key_str);
				if (_inst != -1)
				{
					instance_create_layer(obj_player.x + 30, obj_player.y, "Instances", _inst);
					debug_return = "Spawned " + _key_str;
				}
				else
				{
					debug_return = "Error: instance \"" + _key_str + "\" does not exist.";
				}
			}
			var _str = "room ";
			if (string_pos(_str, _key_str))
			{
				_key_str = string_replace(_key_str, _str, "");
				var _room = asset_get_index(_key_str);
				if (_room != -1)
				{
					room_goto(_room);
					debug_return = "Moved to " + _key_str;
				}
				else
				{
					debug_return = "Error: room \"" + _key_str + "\" does not exist.";
				}
			}
			var _str = "variable_set ";
			if (string_pos(_str, _key_str))
			{
				// Parse for valid object
				_key_str = string_replace(_key_str, _str, "");
				var _dot_index = string_pos(".", _key_str);
				var _obj = string_copy(_key_str, 1, _dot_index - 1); // Object string
				var _obj_index = asset_get_index(_obj); // Object index
				var _inst = instance_find(_obj_index, 0); // Target instance
				if (is_undefined(_obj_index))
				{
					debug_return = "Error: " + _obj + " not found.";
					exit;
				}
				
				// Parse for variable
				_key_str = string_replace(_key_str, _obj + ".", "");
				var _space_index = string_pos(" ", _key_str);
				var _var = string_copy(_key_str, 1, _space_index - 1);
				if (not variable_instance_exists(_inst, _var))
				{
					debug_return = "Error: " + _var + " not found in " + _obj + ".";
					exit;
				}
				
				// Parse for value
				_key_str = string_replace(_key_str, _var + " ", "");
				var _val = _key_str;
				
				// Check if value is a real
				if (string_digits(_val) == _val)
				{
					_val = real(_val);
				}
				
				// Set variable
				variable_instance_set(_inst, _var, _val);
				
				// Cast the val back to a string for the return
				_val = string(_val);
				
				// Return
				debug_return = "Set " + _var + " in " + _obj + " to " + _val + ".";
			}
		
			// Raw commands
			switch (keyboard_string)
			{
				case "time_day":
					global.sky_color = global.day_color;
					debug_return = "Set the time to day.";
					break;
			
				case "time_night":
					global.sky_color = global.night_color;
					debug_return = "Set the time to night.";
					break;
			
				case "god_mode":
					obj_player.god_mode = !obj_player.god_mode;
					debug_return = obj_player.god_mode ? "God mode enabled." : "God mode disabled."
					break;
					
				case "debug_mode":
					global.debug = !global.debug;
					debug_return = global.debug ? "Debug mode enabled." : "Debug mode disabled (game must be restarted to re-enable).";
				
				case "save_game":
					debug_return = save_game();
					break;
				
				case "load_game":
					debug_return = load_game(obj_player.name + ".sav");
					break;
				
				case "room_restart":
					room_restart();
					debug_return = "Restarted room.";
					break;
			
				case "game_restart":
					game_restart();
					debug_return = "Restarted game.";
					break;
				
				case "game_end":
					game_end();
					debug_return = "This really shouldn't be neccesary...";
					break;
			}
			keyboard_string = "";
		}
	}
}