if (live_call()) return live_result;

// Box
var _x = (global.view_w / 2) - ((width * size) / 2);
var _y = global.view_h - (height * size);
draw_sprite_part(spr_textbox, 0, 0, 0, size, size, _x, _y);
for (var i = 1; i < width - 1; i++;) {
	draw_sprite_part(spr_textbox, 0, size, 0, size, size, _x + size * i, _y);
}
draw_sprite_part(spr_textbox, 0, size * 2, 0, size, size, _x + size * (width - 1), _y);
draw_sprite_part(spr_textbox, 0, 0, size * 2, size, size, _x, _y + size);
for (var i = 1; i < width - 1; i++;) {
	draw_sprite_part(spr_textbox, 0, size, size * 2, size, size, _x + size * i, _y + size);
}
draw_sprite_part(spr_textbox, 0, size * 2, size * 2, size, size, _x + size * (width - 1), _y + size);

// Text
draw_set_color(pal_white);
draw_set_font(fnt_main);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
var _str = string_copy(text_list[| index], 0, count); 
draw_text_ext(_x + 7, _y + 4, _str, 8, (size * width) - 10);
count += 0.5;
if (count > string_length(text_list[| index])) {
	count = string_length(text_list[| index]);
}