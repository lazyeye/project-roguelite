if (live_call()) return live_result;

// Text progression
if (pressed(ord("J"))) {
	if (count != string_length(text_list[| index])) {
		count = string_length(text_list[| index]);
	} else {
		count = 0;
		index++;
		if (index > ds_list_size(text_list) - 1) {
			instance_destroy();
			parent.data_ref[? parent.object_ref] = end_key;
			global.text_disable = 5;
		}
	}
}