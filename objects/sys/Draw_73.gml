// @description Lighting

// Light Surface
if(not surface_exists(light_surf))
{
	light_surf = surface_create(global.view_w, global.view_h);
}
surface_set_target(light_surf)
{
	draw_clear_alpha(0, 1);
	
	gpu_set_colorwriteenable(true, true, true, false);
	gpu_set_blendmode(bm_add);
	with (par_light)
	{
		draw_sprite_ext(spr_light, 0, x - global.view_x, y - global.view_y, 2, 2, 0, light_color, 0.35);
	}
	gpu_set_blendmode(bm_normal);
	gpu_set_colorwriteenable(true, true, true, true);
}
surface_reset_target();

// Overlay (combination of sky color and lights)
if(!surface_exists(overlay_surf))
{
	overlay_surf = surface_create(global.view_w, global.view_h);
}
surface_set_target(overlay_surf)
{
	draw_clear_alpha(global.sky_color, 1);
	gpu_set_blendmode(bm_add);
    draw_surface(light_surf, 0, 0);
    gpu_set_blendmode(bm_normal);
}
surface_reset_target();

// Draw Surface
gpu_set_blendmode_ext(bm_dest_color, bm_zero);
draw_surface(overlay_surf, global.view_x, global.view_y);
gpu_set_blendmode(bm_normal);

