#region Display Settings

var _file = file_text_open_write("test.json")
file_text_write_string(_file, "test");
file_text_close(_file);

// Resolutions
resolutions = ds_list_create();

res_1 = ds_map_create();
ds_map_add(res_1, "Name", "1280 x 720");
ds_map_add(res_1, "Width", 1280);
ds_map_add(res_1, "Height", 720);
ds_list_add(resolutions, res_1);

res_2 = ds_map_create();
ds_map_add(res_2, "Name", "1920 x 1080");
ds_map_add(res_2, "Width", 1920);
ds_map_add(res_2, "Height", 1080);
ds_list_add(resolutions, res_2);

res_3 = ds_map_create();
ds_map_add(res_3, "Name", "2560 x 1440");
ds_map_add(res_3, "Width", 2560);
ds_map_add(res_3, "Height", 1440);
ds_list_add(resolutions, res_3);

resolution_curr = res_2;
ideal_width = 0;
ideal_height = 135;
zoom_time = 0;
display_width = display_get_width();
display_height = display_get_height()

aspect_ratio = display_width / display_height;

ideal_width = round(ideal_height * aspect_ratio);
// ideal_height=round(ideal_width / aspect_ratio);

// Perfect Pixel Scaling
if display_width mod ideal_width != 0
{
  var d = round(display_width / ideal_width);
  ideal_width = display_width / d;
}
if display_height mod ideal_height != 0
{
  var d = round(display_height / ideal_height);
  ideal_height = display_height / d;
}

// Check for odd numbers
if(ideal_width & 1)
  ideal_width++;
if(ideal_height & 1)
  ideal_height++;

window_set_size(resolution_curr[? "Width"], resolution_curr[? "Height"]);
display_set_gui_size(ideal_width, ideal_height);
surface_resize(application_surface, resolution_curr[? "Width"], resolution_curr[? "Height"]);

camera = camera_create();
global.view_x = 0;
global.view_y = 0;
global.view_w = ideal_width;
global.view_h = ideal_height;
camera_ticks = 0;

global.shake[0] = false;
global.shake[1] = 0;

room_goto(room_next(room));
window_center();
alarm[0] = 1;

#endregion

#region Debug

global.debug = true;
instance_create_depth(0, 0, 0, obj_debug);

#endregion

#region Encryption

// Extract Game Encryption Key
if (file_exists("game.dat"))
{
	var _map = json_secure_load("game.dat", 54920006);
	global.crypt_key = _map[? "key"];
	ds_map_destroy(_map);
	show_debug_message("Loaded encryption key: " + string(global.crypt_key));
}
else
{
	// Create Game Encryption Key
	var _original_seed = random_get_seed(); // Game seed
	randomize(); // Create unique key for user
	global.crypt_key = random_get_seed(); // Assign unique key for save files
	var _map = ds_map_create();
	ds_map_set(_map, "key", global.crypt_key);
	json_secure_save(_map, "game.dat", 54920006);
	ds_map_destroy(_map);
	random_set_seed(_original_seed); // Revert to game seed
	show_debug_message("Set an encryption key: " + string(global.crypt_key));
}

#endregion

#region Databases

// Animation JSON
var buff = buffer_load("cc_data.json");
cc_data = json_decode(buffer_read(buff, buffer_text));
buffer_delete(buff);
var _hair_map = cc_data[? "Hair"];
global.cc_hair_list = ds_list_create_from_map(_hair_map);
var _body_map = cc_data[? "Body"];
global.cc_body_list = ds_list_create_from_map(_body_map);

// Palette system
pal_swap_init_system(shd_pal_swapper, true);

world_data = ds_map_create();
var _map = ds_map_create();
ds_map_add_map(world_data, "npc_keys", _map);

global.save_data = ds_map_create();
ds_map_add_multiple(global.save_data,
"name", "null",
"hair", "null",
"hair_color", "null",
"body", "null",
"body_color", "null",
"room", "null",
"x", "null",
"y", "null",
"npc_keys", "null");

#endregion

#region Variables

// Death
death[0] = false; // bool
death[1] = 0; // ticks
death_sprite[0] = noone; // index
death_sprite[1] = 0; // sub_image
death_spirte[2] = 0; // x
death_sprite[3] = 0; // y

// Text
global.text_disable = false;

#endregion

#region Lighting

light_surf = -1;
overlay_surf = -1;
global.day_color = c_white;
global.night_color = $442b26;
global.sky_color = global.day_color;

#endregion