{
    "id": "943e6765-b4b3-4206-8577-3d17d3724f7e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sys",
    "eventList": [
        {
            "id": "013845c8-526e-439d-ba25-01b721cb98ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "42824741-01db-46e2-8f75-46735387a6d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "ac96a8f4-73ea-4658-9ccd-383e1e395f21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "e70534e3-ffd9-473a-b443-318c1d4d36a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "2ab41620-3d7f-4bd7-926e-34481c7bab63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 7,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "31a64bdf-9eb3-4ccc-9cec-d85de363648e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "10202885-0daf-4948-a792-022792ec417a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "fbccf4dc-aa2a-45b5-85f7-5d0d8c65eedf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "b98bbd79-14b2-4493-b6f6-4459345d7b8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        },
        {
            "id": "93f82eea-3627-48e3-a2ff-5afd7ebab2e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "943e6765-b4b3-4206-8577-3d17d3724f7e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}