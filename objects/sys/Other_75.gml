// Debug messages to show the event triggered and the pad associated with it
show_debug_message("Event = " + async_load[? "event_type"]);
show_debug_message("Pad = " + string(async_load[? "pad_index"]));

switch(async_load[? "event_type"])
{
	case "gamepad discovered":
		
		var _pad = async_load[? "pad_index"];
		gamepad_set_axis_deadzone(_pad, 0.2);
		gamepad_set_button_threshold(_pad, 0.1);
		if instance_exists(obj_player)
		{
			with obj_player
			{
				if pad == noone
				{
					pad = _pad;
					print("Gamepad " + string(_pad) + " assigned.");
					break;
				}
			}
		}
		
	break;
	
	case "gamepad lost":
	
		var _pad = async_load[? "pad_index"];
		if instance_exists(obj_player)
		{
			with obj_player
			{
				if pad == _pad
				{
					pad = noone;
					print("Gamepad " + string(_pad) + " unassigned.");
					break;
				}
			}
		}
	
	break;	
}
			