#region Death

if (death[0])
{
	// Black screen
	draw_set_color(c_black);
	draw_rectangle(global.view_x, global.view_y, global.view_x + global.view_w, global.view_y + global.view_h, false);
	
	// Draw player
	draw_sprite(death_sprite[0], death_sprite[1], death_sprite[2], death_sprite[3]);
	
	//Draw player reflection
	var _y = death_sprite[3] + sprite_get_height(death_sprite[0]);
	draw_sprite_ext(death_sprite[0], death_sprite[1], death_sprite[2], _y, 1, -1, 0, c_white, 0.2);
	exit;
}

#endregion

#region Depth

depth_system_draw("Props");

#endregion