// Resolution Swap
if pressed(key_z)
{	
	// Test Resolution
	var i = ds_list_find_index(resolutions, resolution_curr);
	do
	{
		i++;
		i = wrap(i, 0, ds_list_size(resolutions) - 1);
		var test_res = resolutions[| i];
	}
	until test_res[? "Width"] <= display_get_width();
	resolution_curr = resolutions[| i];

	// Set Resolution
	window_set_size(resolution_curr[? "Width"], resolution_curr[? "Height"]);
	surface_resize(application_surface, resolution_curr[? "Width"], resolution_curr[? "Height"]);
	alarm[0] = 1;
}

// Follow Player
if instance_exists(obj_player)
{
	global.view_x = lerp(global.view_x, obj_player.x - global.view_w / 2, 0.25);
	global.view_y = lerp(global.view_y, obj_player.y - global.view_h / 2, 0.25);
}

// Default Camera View
else
{
	global.view_x = global.view_x;
	global.view_y = global.view_y;
}

// Shake
if (global.shake[0])
{
	if (global.shake[1] <= 0)
	{
		global.shake[0] = false;
		global.shake[1] = 0;
	}
	global.view_x += irandom_range(-5, 5);
	global.view_y += irandom_range(-5, 5);
	global.shake[1]--;
}

camera_set_view_size(view_camera[0], global.view_w, global.view_h);
var _round = global.view_w / surface_get_width(application_surface);
camera_set_view_pos(view_camera[0], round_n(global.view_x, _round), round_n(global.view_y, _round));

