{
    "id": "4b5b16b2-109e-4586-b4b4-29ede79b1e2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_depth_npc",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4afbc66-236f-4beb-8523-722e5ed97479",
    "visible": true
}