// DEMO: Movement
hsp = 2 *(check(key_d) - check(key_a));
vsp = 2 * (check(key_s) - check(key_w));

if (hsp > 0) var _bbox_side = bbox_right; else _bbox_side = bbox_left;
if (tilemap_get_at_pixel(collision_map, _bbox_side + hsp, bbox_top) or tilemap_get_at_pixel(collision_map, _bbox_side + hsp, bbox_bottom))
{
	hsp = 0;
}

if (vsp > 0) var _bbox_side = bbox_bottom; else _bbox_side = bbox_top;
if (tilemap_get_at_pixel(collision_map, bbox_left, _bbox_side + vsp) or tilemap_get_at_pixel(collision_map, bbox_right, _bbox_side + vsp))
{
	vsp = 0;
}
x += hsp;
y += vsp;