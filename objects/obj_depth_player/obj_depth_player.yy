{
    "id": "694ac334-8e40-44f0-8808-8d83a6fb1183",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_depth_player",
    "eventList": [
        {
            "id": "07558679-061b-41a1-bca6-3c9edd6f3a29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "694ac334-8e40-44f0-8808-8d83a6fb1183"
        },
        {
            "id": "8e15a565-6eee-4eed-abb6-ca93a024f1d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "694ac334-8e40-44f0-8808-8d83a6fb1183"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f56c1eb1-a9bd-48a1-8cf0-e3cc52b5ab9d",
    "visible": true
}