// Interaction
if (not instance_exists(obj_textbox) and not global.text_disable) 
{
	if (pressed(ord("J"))) 
	{
		if (point_distance(x, y, obj_player.x, obj_player.y) < 20) 
		{
			dia_start();
			event_user(0); // Note! The NPC passes a key to the textbox to update once it is completed.
		}
	}
}

depth = -bbox_bottom;
