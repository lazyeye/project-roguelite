{
    "id": "fa306f6b-63d5-4fa7-b6f2-9f7d65399294",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "par_npc",
    "eventList": [
        {
            "id": "f6c860cc-20b2-4ca2-b2e8-e3bf642cb5b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa306f6b-63d5-4fa7-b6f2-9f7d65399294"
        },
        {
            "id": "4333e05d-e509-406c-8fba-c4826c778bc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa306f6b-63d5-4fa7-b6f2-9f7d65399294"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dc844da1-7b77-4736-960b-0c9dd662e834",
    "visible": true
}