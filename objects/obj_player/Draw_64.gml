if (live_call()) return live_result;

//HUD Ping
if (hud_ping > 0)
{
	if hud_ping > hud_duration - hud_fade
	{
		var _a = 1;
	}
	else
	{
		var _a = hud_ping / hud_fade;
	}
	hud_ping--;
}
else
{
	var _a = 0;
}

//HP
var _base_w = (sprite_get_width(spr_hp) + 1) * max_hp - 1; // Minus one because the first one has no padding
var _base_x = (global.view_w / 2) - (_base_w / 2);
var _inc = _base_w / max_hp;
for (var i = 0; i < max_hp; i++;)
{
	// Subimage selection
	var _index;
	if i < hp _index = 0;
	else _index = 1;
	
	// Coordinate selection
	var _x = _base_x + (_inc * i);
	var _y = global.view_h - 15;
	
	// Shake
	if (damage[0])
	{
		// Shake
		if (hp - i <= damage[2]) // If the current hp piece is to be damaged
		{
			_x += irandom_range(-2, 2);
			_y += irandom_range(-2, 2);
		}
		
		// Flash
		if (damage[1] mod 2 != 0)
		{
			shader_set(shd_white);
		}
	}
	
	draw_sprite_ext(spr_hp, _index, _x, _y, 1, 1, 0, c_white, _a);
	shader_reset();
}