if (live_call()) return live_result;

#region Debug

if (locked) exit;

if pressed(ord("G"))
{
	ping_hud();
}

#endregion

#region Death

// Note that the death event needs to be run before any other code to ensure that
// values are not updated after being saved here

if (hp <= 0)
{
	with (sys)
	{
		//global.view_x = other.x - global.view_w / 2; // Reset view
		//global.view_y = other.y - global.view_y / 2;
		death[0] = true;
		death_sprite[0] = other.sprite_index;
		death_sprite[1] = other.image_index;
		death_sprite[2] = other.x;
		death_sprite[3] = other.y;
		global.shake[0] = false; // Disable global.shake
		global.shake[1] = 0; // Reset ticks
		instance_deactivate_all(true);
	}
	exit;
}

#endregion

#region Movement

hsp = move_speed * (check(key_d) - check(key_a));
vsp = move_speed * (check(key_s) - check(key_w));

x += hsp;
y += vsp;

#endregion

#region Animation

depth = -bbox_bottom;

// Run Animation
if (check(key_a))
{
	sprite_index = spr_player_run_left;
}
else if (check(key_d))
{
	sprite_index = spr_player_run_right;
}
else if (check(key_w))
{
	sprite_index = spr_player_run_up;
}
else if (check(key_s))
{
	sprite_index = spr_player_run_down;
}

// Idle Animation
if (hsp = 0) and (vsp = 0)
{
	switch (sprite_index)
{
		case spr_player_run_left:
			image_index = 0;
			sprite_index = spr_player_idle_left;
			break;
		case spr_player_run_right:
			image_index = 0;		
			sprite_index = spr_player_idle_right;
			break;
		case spr_player_run_up:
			image_index = 0;
			sprite_index = spr_player_idle_up;
			break;
		case spr_player_run_down:
			image_index = 0;
			sprite_index = spr_player_idle_down;
			break;	
	}
}

#endregion

#region Ticks

if (damage[0])
{
	if (damage[1] < 5) // Five being the amount of global.shakes
	{
		damage[1]++;
	}
	else
	{
		damage[0] = false; // Turn of damage bool
		damage[1] = 0; // Reset ticks
		damage[2] = 0; // Reset damage
	}
}

#endregion