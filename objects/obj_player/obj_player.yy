{
    "id": "6437bbdd-8982-44b7-a7ef-c1cc4b71a2f6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "958c7f14-e1f1-4fa0-aa47-b1e11a66155c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6437bbdd-8982-44b7-a7ef-c1cc4b71a2f6"
        },
        {
            "id": "7a01391f-90f0-468e-9e8a-261a6bf6ed1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6437bbdd-8982-44b7-a7ef-c1cc4b71a2f6"
        },
        {
            "id": "322eaec3-2dc5-42a1-89da-a4f2b191d46c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6437bbdd-8982-44b7-a7ef-c1cc4b71a2f6"
        },
        {
            "id": "3aa96214-2c00-4626-a401-1c2082d44d3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6437bbdd-8982-44b7-a7ef-c1cc4b71a2f6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "54d283dd-01f9-41cf-b2b0-cb4a7c34c664",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7aeba818-278e-401e-b0e3-f501fc6d0441",
    "visible": true
}