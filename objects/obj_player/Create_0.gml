if (live_call()) return live_result;

// Inherit
event_inherited();

// Macro
#macro hud_fade 60
#macro hud_duration seconds(4)

// Variables
dynamic = true;
name = -1;
move_speed = 1.5;
max_hp = 3;
hp = 3;
hud_ping = hud_duration;
locked = false;

// Damage Array
damage[0] = false; // Damage bool
damage[1] = 0; // Damage ticks
damage[2] = 0; // Damage to apply