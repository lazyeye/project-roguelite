
{
    "name": "rm_cc",
    "id": "8463f569-61fb-4d76-b4d2-e0abfcc32d66",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "7504df4f-5885-4fd4-988c-1b3927cea296",
        "b635349b-f0cb-4a5d-a28d-aaf359e9635b",
        "6d3eef1e-435f-44cc-bc04-9e9bbaa0155b",
        "ef5d8f61-388e-430c-bef6-6e9d9e2da42c",
        "017de87b-6621-4023-a31b-1bf69a753a1d",
        "b6dbebb2-7d11-4e50-a33d-073b0f253ed2",
        "08360091-839e-4026-9ff7-2deee202de71",
        "e8a3c7d1-f5a9-4231-bfff-549f227ccf0f",
        "ee01e658-3c7a-409e-b386-06454dff6364"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Buttons",
            "id": "a7c9874d-4d7c-4466-8adb-a113e962b6ef",
            "depth": 0,
            "grid_x": 16,
            "grid_y": 9,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_2420836A","id": "7504df4f-5885-4fd4-988c-1b3927cea296","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2420836A","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "429736d9-61d4-418a-a057-6022983c02d0","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"hair\""}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 123,"y": 66},
{"name": "inst_420DF6D5","id": "b635349b-f0cb-4a5d-a28d-aaf359e9635b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_420DF6D5","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "46c8ffb6-4353-4519-aaaf-b39b6c0a4ae0","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"hair color\""}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 123,"y": 76},
{"name": "inst_6E5B6C2E","id": "6d3eef1e-435f-44cc-bc04-9e9bbaa0155b","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6E5B6C2E","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "4f9039d6-415c-47b3-a3de-afb3926767e5","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"body\""}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 123,"y": 97},
{"name": "inst_4B4041B5","id": "ef5d8f61-388e-430c-bef6-6e9d9e2da42c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4B4041B5","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "cc80e585-dda3-4b70-9afd-8b6e14de5246","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"body color\""}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 123,"y": 107},
{"name": "inst_DC4DA8C","id": "017de87b-6621-4023-a31b-1bf69a753a1d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_DC4DA8C","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "17425c83-01cc-4dbb-8463-2ac7b9db51e6","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"hair\""}],"rotation": 0,"scaleX": -1,"scaleY": 1,"mvc": "1.0","x": 111,"y": 66},
{"name": "inst_5D1CCC23","id": "b6dbebb2-7d11-4e50-a33d-073b0f253ed2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5D1CCC23","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "32e8dfb1-5a35-46e8-b32d-a47c9fa556d6","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"hair color\""}],"rotation": 0,"scaleX": -1,"scaleY": 1,"mvc": "1.0","x": 111,"y": 76},
{"name": "inst_4069A8C3","id": "08360091-839e-4026-9ff7-2deee202de71","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4069A8C3","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "c195dd6c-c102-4c55-b6d1-18e7de032aee","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"body\""}],"rotation": 0,"scaleX": -1,"scaleY": 1,"mvc": "1.0","x": 111,"y": 97},
{"name": "inst_11AF6AE5","id": "e8a3c7d1-f5a9-4231-bfff-549f227ccf0f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_11AF6AE5","objId": "df346c13-c735-4fd2-930f-06c44ef66f9b","properties": [{"id": "41de807e-3422-4f55-af41-951dfd21c77d","modelName": "GMOverriddenProperty","objectId": "df346c13-c735-4fd2-930f-06c44ef66f9b","propertyId": "abe24259-30e3-458c-82e5-b18c39a190d2","mvc": "1.0","value": "\"body color\""}],"rotation": 0,"scaleX": -1,"scaleY": 1,"mvc": "1.0","x": 111,"y": 107}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "System",
            "id": "f4ef415c-27d1-41cb-ad86-d268bca46265",
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_6E97878E","id": "ee01e658-3c7a-409e-b386-06454dff6364","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_6E97878E","objId": "a96b60fa-a370-4925-8537-a7026e8d9923","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 80,"y": 9}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "b27c10b4-305f-4e8b-ac1e-a775ec50dc5f",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4282000422 },
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "a5cdd449-adbb-4dca-bd94-df9016376e46",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "21d137f5-b577-4582-9c8b-fcae1346a31a",
        "Height": 768,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 1024
    },
    "mvc": "1.0",
    "views": [
{"id": "9a7eef77-4927-4ba9-a488-e992180a90d9","hborder": 32,"hport": 768,"hspeed": -1,"hview": 135,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1024,"wview": 240,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e694d76c-8639-4cd5-9bb4-20b2bd2d4be4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d9db2546-490a-43be-ba96-73a1f2d2b45b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "9a558034-abea-4fea-a0e0-bafa73976603","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1f19dad9-6768-4066-9c13-ac1b2dd99902","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "85017ff3-3690-4ef7-ba57-5d360b602e2b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "c6b69af6-dff3-456c-b302-674d79651cdf","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "623bb929-3d4a-453a-9f0b-7d4f404302d3","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "c10b466e-57f4-4da0-8293-d170571a7bf8",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}