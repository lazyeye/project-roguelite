#macro pressed keyboard_check_pressed
#macro released keyboard_check_released
#macro check keyboard_check
#macro key_w ord("W")
#macro key_a ord("A")
#macro key_s ord("S")
#macro key_d ord("D")
#macro key_z ord("Z")
#macro key_r ord("R")
#macro mouse_left mouse_check_button_pressed(mb_left)
#macro mouse_right mouse_check_button_pressed(mb_right)
#macro pal_white $e7e0db