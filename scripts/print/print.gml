/* Print a message to the Console Window. */
// / @desc Print
// / @param Message

//  Set String Variable Argument
var debugString = argument0;

//  Send to console window
show_debug_message(string(debugString));