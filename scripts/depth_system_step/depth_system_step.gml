/// @script dpeth_system_step()
/// @desc Updates the currently selected priority queue to draw

global.depth_x = (global.view_x + (global.view_w / 2)) div cell_size;
global.depth_y = (global.view_y + (global.view_h / 2)) div cell_size;