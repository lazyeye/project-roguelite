/// generate_player_sprites();
/// @description Creates the player sprites.

// Idle
spr_player_idle_right = cc_create_sprite("Idle", 0, 0);
spr_player_idle_down = cc_create_sprite("Idle", 1, 1);
spr_player_idle_left = cc_create_sprite("Idle", 2, 2);
spr_player_idle_up = cc_create_sprite("Idle", 3, 3);

// Run
spr_player_run_right = cc_create_sprite("Run", 0, 3);
spr_player_run_down = cc_create_sprite("Run", 4, 7);
spr_player_run_left = cc_create_sprite("Run", 8, 11);
spr_player_run_up = cc_create_sprite("Run", 12, 15);