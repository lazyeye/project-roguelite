/// @script depth_system_init()
/// @desc Initalizes the depth sorting system

// Macros
#macro cell_size 100

// Enum
enum e_draw
{
	id, // returns noone if it is a sprite
	index, // sprite_index or object_index
	x, // x coordinate (sprites only)
	y, // y coordinate (sprites only)
	bot // the bottom of the sprite (sprites only)
}

// Variables
global.draw_on = false;
global.depth_x = 0;
global.depth_x = 0;