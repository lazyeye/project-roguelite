/// ds_map_write_multiple(ID, key, value...)
/// @description Updates multiple values in a map.
/// @param {real} _map_index
/// @param {string} _key
/// @param {*} _value

var _map = argument[0];
for (var i = 1; i < argument_count; i++;)
{
	var _key = argument[i];
	i++;
	var _value = argument[i];
	_map[? _key] = _value;
}
