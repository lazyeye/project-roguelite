/// @description Applies the given damage to the player.
/// @param {real} hp

with (obj_player)
{
	if not (god_mode)
	{
		damage[0] = true; // Bool
		damage[1] = 0; // Ticks
		damage[2] = abs(argument0);
		hp -= damage[2]; // Subtract HP
		global.shake[0] = true;
	}
}