/// json_load()
/// @description Loads the JSON file and returns a new map with its data.
/// @param {string} _file

var _map = ds_map_create();
var _buff = buffer_load(argument0);
_map = json_decode(buffer_read(_buff, buffer_text));
buffer_delete(_buff);
return _map;