/// @description Adds a map to a list
/// @param list_index
/// @param map_index

ds_list_add(argument0, argument1);
var _index = ds_list_find_index(argument0, argument1);
ds_list_mark_as_map(argument0, _index);