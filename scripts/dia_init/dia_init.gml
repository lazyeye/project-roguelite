//@desc Inits the dia map, adding the NPC's key to the database and assigning the instance's data_ref value.
//@param {string} name

dia = ds_map_create();
if (not ds_map_exists(global.save_data[? "npc_keys"], argument0)) 
{
	ds_map_add(global.save_data[? "npc_keys"], argument0, "null");
}
data_ref = global.save_data[? "npc_keys"];