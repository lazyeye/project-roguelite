/// @description Returns an array translated from an array, with the option to destroy the grid afterwords.
/// @param {real} _grid
/// @param {bool} _destroy_grid

// Translate grid into array for faster performance
var _array;
for (var i = 0; i < ds_grid_width(argument0); i++;)
{
	for (var j = 0; j < ds_grid_height(argument0); j++;)
	{
		_array[i, j] = argument0[# i, j];
	}
}

// Destory grid
if (argument1)
{
	ds_grid_destroy(argument0);
}

return _array;