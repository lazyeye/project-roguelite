/// @script depth_system_draw
/// @desc Draws the generated priority queues for depth drawing

// Check Layer
if (not layer_exists(argument0)) exit;

// Queue selection
global.draw_on = true;
var _q = ds_priority_create();
ds_priority_copy(_q, global.draw_queues[global.depth_x, global.depth_y]);

// Drawing
while (ds_priority_size(_q) > 0)
{
	var _a = ds_priority_delete_min(_q);
	if (_a[e_draw.id] == noone)
	{
		draw_sprite(_a[e_draw.index], 0, _a[e_draw.x], _a[e_draw.y]);
	}
	else
	{
		with (_a[e_draw.id])
		{
			if (dynamic)
			{
				event_perform_object(_a[e_draw.index], ev_draw, 0);
			}
		}
	}
}
ds_priority_destroy(_q);
global.draw_on = false;