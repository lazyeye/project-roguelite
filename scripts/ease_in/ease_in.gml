/// @function ease(start, end, time, duration)
/// @param {real} start
/// @param {real} end
/// @param {real} time
/// @param {real} duration

var _start, _end, _time, _duration;
_start = argument0;
_end = argument1;
_time = argument2;
_duration = argument3;

var _percent = _time/_duration;
_percent = 1 - _percent;
_percent = _percent * _percent;
_percent = 1 - _percent;
return lerp(_start, _end, _percent);