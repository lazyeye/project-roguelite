// /@function ease(start, end, time, duration)
// /@arg start
// /@arg end
// /@arg time
// /@arg duration
var _start, _end, _time, _duration;
_start = argument0;
_end = argument1;
_time = argument2;
_duration = argument3;

var _percent = _time/_duration;
_percent = _percent * _percent;
return lerp(_start, _end, _percent);