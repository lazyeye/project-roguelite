// /@description Calculates the direction (angle) between the mouse and the given coordinates.
// /@param x
// /@param y

return point_direction(argument0, argument1, mouse_x, mouse_y);