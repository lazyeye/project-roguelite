/// save_game()
/// @description Saves the game data to a file.

// Player Data
with (obj_player)
{
	// Clear the map for good measure
	ds_map_clear(global.save_data);
	
	// Add save info to save file
	ds_map_write_multiple(global.save_data,
	"name", name,
	"hair", hair,
	"hair_color", hair_color,
	"body", body,
	"body_color", body_color,
	"x", x,
	"y", y);
}

// System data
with (sys)
{
	ds_map_write_multiple(global.save_data,
	"room", room_get_name(room),
	"global.sky_color", global.sky_color,
	"global.view_x", global.view_x,
	"global.view_y", global.view_y);
	
	ds_map_add_map(global.save_data, "npc_keys", world_data[? "npc_keys"])
}

if (global.debug)
{
	json_save(global.save_data, obj_player.name + ".sav");
}
else
{
	json_secure_save(global.save_data, obj_player.name + ".sav", global.crypt_key);
}

return "Succesfully saved game to " + obj_player.name +".sav.";
