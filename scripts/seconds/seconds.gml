// /@function seconds(seconds);
// /@desc     returns the given number of seconds in steps
// /@param    {Real} seconds

return argument0 * room_speed;