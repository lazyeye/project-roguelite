/// @function depth_system_process(_layer)
/// @desc Processes the given layer for depth sorting
/// @param {sting} _layer

// Check Layer
if (not layer_exists(argument0)) exit;

// Init
var _sprite_array = layer_get_all_elements(argument0);
var _len = array_length_1d(_sprite_array);

// Room Division
var _cols = ceil(room_width / cell_size);
var _rows = ceil(room_height / cell_size);

// Queue creations
for (var i = 0; i < _cols; i++;)
{
	for (var j = 0; j < _rows; j++;)
	{
		global.draw_queues[i, j] = ds_priority_create();
	}
}

// Sprite organization
for (var i = 0; i < _len; i++;)
{
	// Data retrieval
	var _sprite = _sprite_array[i];
	var _index = layer_sprite_get_index(_sprite);
	var _a = 0; // clear the array for use
	_a[e_draw.id] = noone;
	_a[e_draw.index] = layer_sprite_get_sprite(_sprite);
	_a[e_draw.x] = layer_sprite_get_x(_sprite);
	_a[e_draw.y] = layer_sprite_get_y(_sprite);
	_a[e_draw.bot] = layer_sprite_get_y(_sprite) + sprite_get_height(_a[e_draw.index]);
	
	// Queue assignment
	var _bbox_left = _a[e_draw.x];
	var _bbox_top = _a[e_draw.y];
	var _bbox_right = _a[e_draw.x] + sprite_get_width(_a[e_draw.index]);
	var _bbox_bottom = _a[e_draw.y] + sprite_get_height(_a[e_draw.index]);
	for (var j = 0; j < _cols; j++;)
	{
		for (var k = 0; k < _rows; k++;)
		{
			if rectangle_in_rectangle(
			_bbox_left, _bbox_top, _bbox_right, _bbox_bottom,
			(cell_size * j) - global.view_w,
			(cell_size * k) - global.view_h,
			(cell_size * j + cell_size) + global.view_w,
			(cell_size * k + cell_size) + global.view_h)
			{
				ds_priority_add(global.draw_queues[j, k], _a, _a[e_draw.bot]);
			}
		}
	}
	layer_sprite_destroy(_sprite);
}

// Player Camera Update
// This part should set the camera to whatever it will be set to by default
if (instance_exists(obj_player))
{
	global.view_x = obj_player.x - (global.view_w / 2);
	global.view_y = obj_player.y - (global.view_h / 2);
}

// Current Queue
global.depth_x = (global.view_x + (global.view_w / 2)) div cell_size;
global.depth_y = (global.view_y + (global.view_h / 2)) div cell_size;

// Dynamic instance organization
with (par_depth_instance)
{
	ds_priority_add(global.draw_queues[global.depth_x, global.depth_y], draw_array, bbox_bottom);
}