/// cc_sprite_create(hair, body, first_sub, last_sub);
/// @description Creates and returns a sprite with the given properties
/// @param {string} animation
/// @param {real} first_sub
/// @param {real} last_sub

/// Variable definitions

var _a = argument0;
var _head;
switch (_a)
{
	case "Idle": _head = spr_head_idle; break;
	case "Run": _head = spr_head_run; break;
	case "Death": _head = spr_head_death; break;
}												
var _first_sub = argument1;	
var _last_sub = argument2;
var _w = sprite_get_width(_head);
var _h = sprite_get_height(_head);
var _x_o = sprite_get_width(_head) / 2;
var _y_o = sprite_get_height(_head) / 2;
								
var _hair_assets = global.cc_hair_list[| hair];									
var _hair_sprite = _hair_assets[? _a];
var _hair_pal = asset_get_index(_hair_assets[? "Palette"]);
											
var _body_assets = global.cc_body_list[| body]; 									
var _body_sprite = _body_assets[? _a];
var _body_pal = asset_get_index(_body_assets[? "Palette"]);		
var _s = surface_create(_w, _h);										

// First frame
surface_set_target(_s);													
draw_clear_alpha(c_white, 0);		
pal_swap_set(_body_pal, body_color, false)
{
	draw_sprite(asset_get_index(_body_sprite), _first_sub, 0, 0);
}
pal_swap_reset();
draw_sprite(_head, _first_sub, 0, 0);
pal_swap_set(_hair_pal, hair_color, false)
{
	draw_sprite(asset_get_index(_hair_sprite), _first_sub, 0, 0);
}
pal_swap_reset();
var _sprite = sprite_create_from_surface(_s, 0, 0, _w, _h, false, false, _x_o, _y_o);

// Check if there are more frames
if (_last_sub == _first_sub)
{
	surface_reset_target();
	return _sprite;
}
else if (_last_sub > _first_sub)
{
	for (var i = _first_sub + 1; i <= _last_sub; i++;)
	{
		draw_clear_alpha(c_white, 0);	
		pal_swap_set(_body_pal, body_color, false)
		{
			draw_sprite(asset_get_index(_body_sprite), i, 0, 0);
		}
		pal_swap_reset();
		draw_sprite(_head, i, 0, 0);	
		pal_swap_set(_hair_pal, hair_color, false)
		{
			draw_sprite(asset_get_index(_hair_sprite), i, 0, 0);
		}
		pal_swap_reset();
		sprite_add_from_surface(_sprite, _s, 0, 0, _w, _h, false, false);
	}
	sprite_set_speed(_sprite, 7, spritespeed_framespersecond);
	surface_reset_target();
	return _sprite;
}
else
{
	show_debug_message("Character Customization Error: Last subimage is less than first subimage.")
	surface_reset_target();
}