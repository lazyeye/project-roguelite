//@desc Adds a key / value to the NPC's dia map
//@param {string} key
//@param {string} dialogue

var _key = argument[0];
var _list = ds_list_create();
for (var i = 1; i < argument_count; i++)
{
	ds_list_add(_list, argument[i]);
}
ds_map_add_list(dia, _key, _list);