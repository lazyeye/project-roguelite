/// ds_map_add_multiple(ID, key, value...)
/// @description Adds multiple keys and values to a map.
/// @param {string} _key
/// @param {*} _value

var _map = argument[0];
for (var i = 1; i < argument_count; i++;)
{
	var _key = argument[i];
	i++;
	var _value = argument[i];
	ds_map_add(_map, _key, _value);
}
