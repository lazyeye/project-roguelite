/// @description Adds a list to a list
/// @param list_index
/// @param other_list_index

ds_list_add(argument0, argument1);
var _index = ds_list_find_index(argument0, argument1);
ds_list_mark_as_list(argument0, _index);