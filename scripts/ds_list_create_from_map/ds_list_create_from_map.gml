/// ds_list_create_from_map(map_index)
/// NOTE: This script utilizes the custom scripts: ds_list_add_map and ds_list_add_list
/// @description Creates a list out of a map's contents
/// @param {real} _map

// Indexes 
var _list_index = ds_list_create();
var _map_index = argument0;

// DS testing
if (not ds_exists(_map_index, ds_type_map))
{
	show_debug_message("Error in ds_list_add_map_values: " + string(_map_index) + " does not point to a valid map.");
	exit;
}

// Loop through map
var _key = ds_map_find_first(_map_index);
while (not is_undefined(_map_index[? _key]))
{
	var _value = _map_index[? _key];
	
	// DS testing
	if (ds_exists(_value, ds_type_list))
	{
		ds_list_add_list(_list_index, _value);
	}
	else if (ds_exists(_value, ds_type_map))
	{
		ds_list_add_map(_list_index, _value);
	}
	else
	{
		ds_list_add(_list_index, _value);
	}
	_key = ds_map_find_next(_map_index, _key);
}

// Return the new list
return _list_index;

