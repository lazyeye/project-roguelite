/// load_game()
/// @description Loads the game data from the given file.
/// @param {string} _file

var _file = argument0;
if (global.debug)
{
	global.save_data = json_load(_file);
}
else
{
	global.save_data = json_secure_load(_file, global.crypt_key);
}

// World Update
with (sys)
{
	var _index = asset_get_index(global.save_data[? "room"]);
	room_goto(_index);
	global.sky_color = global.save_data[? "global.sky_color"];
	global.view_x = global.save_data[? "global.view_x"];
	global.view_y = global.save_data[? "global.view_y"];
	save_data[? "npc_keys"] = global.save_data[? "npc_keys"];
}

// Player Update
var _allowed_vars = ds_list_create();
ds_list_add(_allowed_vars,
"name",
"hair",
"hair_color",
"body",
"body_color",
"x",
"y");
var _inst = instance_find(obj_player, 0);
for (var i = 0; i < ds_list_size(_allowed_vars); i++;)
{
	if (variable_instance_exists(_inst, _allowed_vars[| i]))
	{
		variable_instance_set(_inst, _allowed_vars[| i], global.save_data[? _allowed_vars[| i]]);
	}
}

with (obj_player)
{
	generate_player_sprites();
	sprite_index = spr_player_idle_right;
}

return "Succesfully loaded game from " + _file + ".";