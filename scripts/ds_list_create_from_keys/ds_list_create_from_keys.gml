/// ds_list_create_from_keys(map_index)
/// NOTE: This script utilizes the custom scripts: ds_list_add_map and ds_list_add_list
/// @description Creates a list out of a map's top level keys.
/// @param {real} _map

// Indexes 
var _list_index = ds_list_create();
var _map_index = argument0;

// DS testing
if (not ds_exists(_map_index, ds_type_map))
{
	show_debug_message("Error in ds_list_add_map_values: " + string(_map_index) + " does not point to a valid map.");
	exit;
}

// Loop through map
var _key = ds_map_find_first(_map_index);
while (not is_undefined(_map_index[? _key]))
{
	ds_list_add(_list_index, _key);
	_key = ds_map_find_next(_map_index, _key);
}

// Return the new list
return _list_index;

